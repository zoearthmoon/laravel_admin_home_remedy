-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1:3306
-- 產生時間： 
-- 伺服器版本： 10.4.10-MariaDB
-- PHP 版本： 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `home_remedy`
--

-- --------------------------------------------------------

--
-- 資料表結構 `admin_menu`
--

DROP TABLE IF EXISTS `admin_menu`;
CREATE TABLE IF NOT EXISTS `admin_menu` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `order` int(11) NOT NULL DEFAULT 0,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `permission`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Dashboard', 'fa-bar-chart', '/', NULL, NULL, NULL),
(2, 0, 6, 'Admin', 'fa-tasks', '', NULL, NULL, '2020-12-09 00:17:15'),
(3, 2, 7, 'Users', 'fa-users', 'auth/users', NULL, NULL, '2020-12-08 22:52:36'),
(4, 2, 8, 'Roles', 'fa-user', 'auth/roles', NULL, NULL, '2020-12-08 22:52:36'),
(5, 2, 9, 'Permission', 'fa-ban', 'auth/permissions', NULL, NULL, '2020-12-08 22:52:36'),
(6, 2, 10, 'Menu', 'fa-bars', 'auth/menu', NULL, NULL, '2020-12-08 22:52:36'),
(7, 2, 11, 'Operation log', 'fa-history', 'auth/logs', NULL, NULL, '2020-12-08 22:52:36'),
(8, 0, 2, '偏方', 'fa-bars', 'remedys', '偏方', '2020-12-08 01:14:10', '2020-12-09 00:19:49'),
(9, 0, 3, '分類', 'fa-bars', 'rcats', '偏方', '2020-12-08 19:16:13', '2020-12-09 00:19:55'),
(10, 0, 5, '素材', 'fa-bars', 'rmaterials', '偏方', '2020-12-08 22:51:56', '2020-12-09 00:20:12'),
(11, 0, 4, '功效', 'fa-bars', 'rfuns', '偏方', '2020-12-08 22:52:24', '2020-12-09 00:20:07');

-- --------------------------------------------------------

--
-- 資料表結構 `admin_operation_log`
--

DROP TABLE IF EXISTS `admin_operation_log`;
CREATE TABLE IF NOT EXISTS `admin_operation_log` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_operation_log_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=699 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `admin_operation_log`
--

INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-12-06 19:12:26', '2020-12-06 19:12:26'),
(2, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-12-06 19:12:32', '2020-12-06 19:12:32'),
(3, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-06 19:12:45', '2020-12-06 19:12:45'),
(4, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-06 19:12:49', '2020-12-06 19:12:49'),
(5, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-06 19:13:04', '2020-12-06 19:13:04'),
(6, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-12-06 19:14:48', '2020-12-06 19:14:48'),
(7, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-12-06 19:14:56', '2020-12-06 19:14:56'),
(8, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-06 19:15:01', '2020-12-06 19:15:01'),
(9, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-06 19:16:15', '2020-12-06 19:16:15'),
(10, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-06 19:23:20', '2020-12-06 19:23:20'),
(11, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-06 19:23:35', '2020-12-06 19:23:35'),
(12, 1, 'admin/auth/users/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-06 19:23:37', '2020-12-06 19:23:37'),
(13, 1, 'admin/auth/users', 'POST', '127.0.0.1', '{\"username\":\"user1\",\"name\":\"user1\",\"password\":\"user1\",\"password_confirmation\":\"user1\",\"roles\":[null],\"permissions\":[\"3\",null],\"_token\":\"qI7zdnX5hu4aKspBDd8wyJhqUSMQQNS3bGpQCoft\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/users\"}', '2020-12-06 19:23:58', '2020-12-06 19:23:58'),
(14, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2020-12-06 19:23:58', '2020-12-06 19:23:58');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(15, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2020-12-06 19:25:44', '2020-12-06 19:25:44'),
(16, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-06 19:25:51', '2020-12-06 19:25:51'),
(17, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-06 19:26:09', '2020-12-06 19:26:09'),
(18, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-06 19:26:10', '2020-12-06 19:26:10'),
(19, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-06 19:27:15', '2020-12-06 19:27:15'),
(20, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-06 19:27:50', '2020-12-06 19:27:50'),
(21, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-06 19:27:52', '2020-12-06 19:27:52'),
(22, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-06 19:27:55', '2020-12-06 19:27:55'),
(23, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-12-07 21:50:51', '2020-12-07 21:50:51'),
(24, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 21:52:14', '2020-12-07 21:52:14'),
(25, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-07 22:04:21', '2020-12-07 22:04:21'),
(26, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:04:27', '2020-12-07 22:04:27'),
(27, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:04:32', '2020-12-07 22:04:32'),
(28, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:04:45', '2020-12-07 22:04:45'),
(29, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:06:24', '2020-12-07 22:06:24'),
(30, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:06:40', '2020-12-07 22:06:40');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(31, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:09:50', '2020-12-07 22:09:50'),
(32, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:09:53', '2020-12-07 22:09:53'),
(33, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:09:59', '2020-12-07 22:09:59'),
(34, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:11:22', '2020-12-07 22:11:22'),
(35, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:11:27', '2020-12-07 22:11:27'),
(36, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:11:30', '2020-12-07 22:11:30'),
(37, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:11:53', '2020-12-07 22:11:53'),
(38, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:11:54', '2020-12-07 22:11:54'),
(39, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:11:56', '2020-12-07 22:11:56'),
(40, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:12:30', '2020-12-07 22:12:30'),
(41, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:12:31', '2020-12-07 22:12:31'),
(42, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:12:39', '2020-12-07 22:12:39'),
(43, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:12:58', '2020-12-07 22:12:58'),
(44, 1, 'admin/remedys', 'POST', '127.0.0.1', '{\"rname\":\"ttttt\",\"rcontent\":\"teststesttseset\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\\/\"}', '2020-12-07 22:13:02', '2020-12-07 22:13:02');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(45, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '[]', '2020-12-07 22:13:02', '2020-12-07 22:13:02'),
(46, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '[]', '2020-12-07 22:15:44', '2020-12-07 22:15:44'),
(47, 1, 'admin/remedys', 'POST', '127.0.0.1', '{\"rname\":\"tsesettsetes\",\"rcontent\":\"tsesetsetset\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-07 22:15:47', '2020-12-07 22:15:47'),
(48, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '[]', '2020-12-07 22:15:48', '2020-12-07 22:15:48'),
(49, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '[]', '2020-12-07 22:18:26', '2020-12-07 22:18:26'),
(50, 1, 'admin/remedys', 'POST', '127.0.0.1', '{\"rname\":\"iugujy\",\"rcontent\":\"gjyjygjyg\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\"}', '2020-12-07 22:18:28', '2020-12-07 22:18:28'),
(51, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '[]', '2020-12-07 22:18:29', '2020-12-07 22:18:29'),
(52, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '[]', '2020-12-07 22:19:26', '2020-12-07 22:19:26'),
(53, 1, 'admin/remedys', 'POST', '127.0.0.1', '{\"rname\":\"fsdfsd\",\"rcontent\":\"fsdfsdfsdfds\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\"}', '2020-12-07 22:19:28', '2020-12-07 22:19:28'),
(54, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '[]', '2020-12-07 22:19:28', '2020-12-07 22:19:28'),
(55, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '[]', '2020-12-07 22:27:29', '2020-12-07 22:27:29'),
(56, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '[]', '2020-12-07 22:27:50', '2020-12-07 22:27:50'),
(57, 1, 'admin/remedys', 'POST', '127.0.0.1', '{\"rname\":\"gfgdgfd\",\"rcontent\":\"gfdgfdgfdg\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\"}', '2020-12-07 22:27:56', '2020-12-07 22:27:56');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(58, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:27:56', '2020-12-07 22:27:56'),
(59, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:28:26', '2020-12-07 22:28:26'),
(60, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:28:33', '2020-12-07 22:28:33');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(61, 1, 'admin/remedys', 'POST', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"rcontent\":\"\\u53f0\\u7063\\u8d85\\u56b4\\u9694\\u96e2\\u6aa2\\u75ab\\u300c\\u51fa\\u623f8\\u79d2\\u906d\\u7f7010\\u842c\\u300d\\u767b\\u4e0aCNN\\r\\n113\\r\\n\\u83ef\\u8996\\r\\n\\u83ef\\u8996\\r\\n2020\\u5e7412\\u67088\\u65e5 \\u9031\\u4e8c \\u4e0a\\u534810:10 [GMT+8]\\u00b72 \\u5206\\u9418 (\\u95b1\\u8b80\\u6642\\u9593)\\r\\n\\r\\n\\u25b2 \\u6a5f\\u5834\\u6aa2\\u75ab\\u793a\\u610f\\u5716\\u3002\\uff08\\u8cc7\\u6599\\u7167\\u7247\\uff09\\r\\n\\r\\n\\/ \\u6731\\u57f9\\u59a4 \\u7d9c\\u5408\\u5831\\u5c0e\\r\\n\\r\\n\\u6b66\\u6f22\\u80ba\\u708e\\uff08COVID-19\\uff0c2019\\u65b0\\u578b\\u51a0\\u72c0\\u75c5\\u6bd2\\uff09\\u75ab\\u60c5\\u8086\\u8650\\u5168\\u7403\\uff0c\\u800c\\u53f0\\u7063\\u76f8\\u5c0d\\u60c5\\u6cc1\\u8da8\\u7de9\\uff0c\\u9632\\u75ab\\u6709\\u6210\\u6709\\u76ee\\u5171\\u7779\\uff1b\\u65e5\\u524d\\u53f0\\u7063\\u70ba\\u9632\\u5835\\u79fb\\u5de5\\u6210\\u70ba\\u9632\\u75ab\\u7834\\u53e3\\uff0c\\u9ad8\\u96c4\\u5e02\\u885b\\u751f\\u5c40\\u52a0\\u5f37\\u7a3d\\u67e5\\u9632\\u75ab\\u65c5\\u9928\\uff0c\\u7d50\\u679c\\u4e00\\u540d\\u83f2\\u7c4d\\u79fb\\u5de5\\u8d70\\u51fa\\u623f\\u9580\\u77ed\\u77ed8\\u79d2\\u9418\\u5c31\\u88ab\\u958b\\u7f7010\\u842c\\u5143\\u7f70\\u55ae\\uff0c\\u800c\\u53f0\\u7063\\u7684\\u300c\\u8d85\\u56b4\\u6aa2\\u75ab\\u300d\\u4e5f\\u767b\\u4e0a\\u7f8e\\u570bCNN\\u7db2\\u7ad9\\u982d\\u7248\\u3002\\r\\n\\r\\n\\u83f2\\u7c4d\\u79fb\\u5de5\\u6aa2\\u75ab\\u6e9c\\u51fa\\u623f8\\u79d2 \\u906d\\u885b\\u751f\\u5c40\\u7f70\\u6b3e10\\u842c\\r\\n\\u65e5\\u524d\\u570b\\u5167\\u5883\\u5916\\u79fb\\u5165\\u75c5\\u4f8b\\u589e\\u52a0\\uff0c\\u70ba\\u4e86\\u9632\\u5835\\u79fb\\u5de5\\u6210\\u70ba\\u9632\\u75ab\\u7834\\u53e3\\uff0c\\u9ad8\\u96c4\\u5e02\\u885b\\u751f\\u5c40\\u52a0\\u5f37\\u7a3d\\u67e5\\u9632\\u75ab\\u65c5\\u9928\\uff0c\\u5f9e11\\u67081\\u65e5\\u5230\\u76ee\\u524d\\u70ba\\u6b62\\uff0c\\u5171\\u67e5\\u523019\\u4eba\\u9055\\u898f\\u5916\\u51fa\\uff0c\\u5305\\u62ec\\u570b\\u969b\\u79fb\\u5de5\\u67099\\u540d\\uff0c\\u5176\\u4e2d\\uff0c\\u4e00\\u540d\\u83f2\\u5f8b\\u8cd3\\u7c4d\\u79fb\\u5de5\\u56e0\\u70ba\\u5077\\u6e9c\\u51fa\\u623f\\u9580\\u77ed\\u77ed8\\u79d2\\u9418\\uff0c\\u60f3\\u62ff\\u6771\\u897f\\u7d66\\u9694\\u58c1\\u623f\\u7684\\u670b\\u53cb\\uff0c\\u9055\\u898f\\u884c\\u70ba\\u5168\\u90fd\\u88ab\\u76e3\\u8996\\u5668\\u9304\\u4e0b\\uff0c\\u9ad8\\u96c4\\u5e02\\u885b\\u751f\\u5c40\\u4e5f\\u6c7a\\u5b9a\\u5c0d\\u8a72\\u540d\\u6f01\\u5de5\\u958b\\u51fa10\\u842c\\u5143\\u7f70\\u55ae\\u3002\\r\\n\\r\\n\\u800c\\u9019\\u6a23\\u7684\\u56b4\\u683c\\u6aa2\\u75ab\\u7f70\\u5247\\uff0c\\u4e5f\\u767b\\u4e0a\\u7f8e\\u570b\\u5a92\\u9ad4CNN\\u7db2\\u7ad9\\u7684\\u982d\\u7248\\uff0c\\u5831\\u5c0e\\u4ee5\\u300c\\u7537\\u5b50\\u6d89\\u5acc\\u7834\\u58de\\u53f0\\u7063\\u9694\\u96e2\\u6aa2\\u75ab8\\u79d2\\uff0c\\u906d\\u7f70\\u6b3e3500\\u7f8e\\u5143\\u300d\\u70ba\\u984c\\uff0c\\u63cf\\u8ff0\\u65e5\\u524d\\u9ad8\\u96c4\\u5e02\\u83f2\\u7c4d\\u79fb\\u5de5\\u88ab\\u7f70\\u6b3e\\u7684\\u4e8b\\u4ef6\\u3002\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-07 22:28:56', '2020-12-07 22:28:56');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(62, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '[]', '2020-12-07 22:28:56', '2020-12-07 22:28:56'),
(63, 1, 'admin/remedys', 'POST', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"rcontent\":\"999\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\"}', '2020-12-07 22:32:28', '2020-12-07 22:32:28'),
(64, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:32:29', '2020-12-07 22:32:29'),
(65, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:32:32', '2020-12-07 22:32:32');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(66, 1, 'admin/remedys/2', 'PUT', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"rcontent\":\"\\u53f0\\u7063\\u8d85\\u56b4\\u9694\\u96e2\\u6aa2\\u75ab\\u300c\\u51fa\\u623f8\\u79d2\\u906d\\u7f7010\\u842c\\u300d\\u767b\\u4e0aCNN\\r\\n113\\r\\n\\u83ef\\u8996\\r\\n\\u83ef\\u8996\\r\\n2020\\u5e7412\\u67088\\u65e5 \\u9031\\u4e8c \\u4e0a\\u534810:10 [GMT+8]\\u00b72 \\u5206\\u9418 (\\u95b1\\u8b80\\u6642\\u9593)\\r\\n\\r\\n\\u25b2 \\u6a5f\\u5834\\u6aa2\\u75ab\\u793a\\u610f\\u5716\\u3002\\uff08\\u8cc7\\u6599\\u7167\\u7247\\uff09\\r\\n\\r\\n\\/ \\u6731\\u57f9\\u59a4 \\u7d9c\\u5408\\u5831\\u5c0e\\r\\n\\r\\n\\u6b66\\u6f22\\u80ba\\u708e\\uff08COVID-19\\uff0c2019\\u65b0\\u578b\\u51a0\\u72c0\\u75c5\\u6bd2\\uff09\\u75ab\\u60c5\\u8086\\u8650\\u5168\\u7403\\uff0c\\u800c\\u53f0\\u7063\\u76f8\\u5c0d\\u60c5\\u6cc1\\u8da8\\u7de9\\uff0c\\u9632\\u75ab\\u6709\\u6210\\u6709\\u76ee\\u5171\\u7779\\uff1b\\u65e5\\u524d\\u53f0\\u7063\\u70ba\\u9632\\u5835\\u79fb\\u5de5\\u6210\\u70ba\\u9632\\u75ab\\u7834\\u53e3\\uff0c\\u9ad8\\u96c4\\u5e02\\u885b\\u751f\\u5c40\\u52a0\\u5f37\\u7a3d\\u67e5\\u9632\\u75ab\\u65c5\\u9928\\uff0c\\u7d50\\u679c\\u4e00\\u540d\\u83f2\\u7c4d\\u79fb\\u5de5\\u8d70\\u51fa\\u623f\\u9580\\u77ed\\u77ed8\\u79d2\\u9418\\u5c31\\u88ab\\u958b\\u7f7010\\u842c\\u5143\\u7f70\\u55ae\\uff0c\\u800c\\u53f0\\u7063\\u7684\\u300c\\u8d85\\u56b4\\u6aa2\\u75ab\\u300d\\u4e5f\\u767b\\u4e0a\\u7f8e\\u570bCNN\\u7db2\\u7ad9\\u982d\\u7248\\u3002\\r\\n\\r\\n\\u83f2\\u7c4d\\u79fb\\u5de5\\u6aa2\\u75ab\\u6e9c\\u51fa\\u623f8\\u79d2 \\u906d\\u885b\\u751f\\u5c40\\u7f70\\u6b3e10\\u842c\\r\\n\\u65e5\\u524d\\u570b\\u5167\\u5883\\u5916\\u79fb\\u5165\\u75c5\\u4f8b\\u589e\\u52a0\\uff0c\\u70ba\\u4e86\\u9632\\u5835\\u79fb\\u5de5\\u6210\\u70ba\\u9632\\u75ab\\u7834\\u53e3\\uff0c\\u9ad8\\u96c4\\u5e02\\u885b\\u751f\\u5c40\\u52a0\\u5f37\\u7a3d\\u67e5\\u9632\\u75ab\\u65c5\\u9928\\uff0c\\u5f9e11\\u67081\\u65e5\\u5230\\u76ee\\u524d\\u70ba\\u6b62\\uff0c\\u5171\\u67e5\\u523019\\u4eba\\u9055\\u898f\\u5916\\u51fa\\uff0c\\u5305\\u62ec\\u570b\\u969b\\u79fb\\u5de5\\u67099\\u540d\\uff0c\\u5176\\u4e2d\\uff0c\\u4e00\\u540d\\u83f2\\u5f8b\\u8cd3\\u7c4d\\u79fb\\u5de5\\u56e0\\u70ba\\u5077\\u6e9c\\u51fa\\u623f\\u9580\\u77ed\\u77ed8\\u79d2\\u9418\\uff0c\\u60f3\\u62ff\\u6771\\u897f\\u7d66\\u9694\\u58c1\\u623f\\u7684\\u670b\\u53cb\\uff0c\\u9055\\u898f\\u884c\\u70ba\\u5168\\u90fd\\u88ab\\u76e3\\u8996\\u5668\\u9304\\u4e0b\\uff0c\\u9ad8\\u96c4\\u5e02\\u885b\\u751f\\u5c40\\u4e5f\\u6c7a\\u5b9a\\u5c0d\\u8a72\\u540d\\u6f01\\u5de5\\u958b\\u51fa10\\u842c\\u5143\\u7f70\\u55ae\\u3002\\r\\n\\r\\n\\u800c\\u9019\\u6a23\\u7684\\u56b4\\u683c\\u6aa2\\u75ab\\u7f70\\u5247\\uff0c\\u4e5f\\u767b\\u4e0a\\u7f8e\\u570b\\u5a92\\u9ad4CNN\\u7db2\\u7ad9\\u7684\\u982d\\u7248\\uff0c\\u5831\\u5c0e\\u4ee5\\u300c\\u7537\\u5b50\\u6d89\\u5acc\\u7834\\u58de\\u53f0\\u7063\\u9694\\u96e2\\u6aa2\\u75ab8\\u79d2\\uff0c\\u906d\\u7f70\\u6b3e3500\\u7f8e\\u5143\\u300d\\u70ba\\u984c\\uff0c\\u63cf\\u8ff0\\u65e5\\u524d\\u9ad8\\u96c4\\u5e02\\u83f2\\u7c4d\\u79fb\\u5de5\\u88ab\\u7f70\\u6b3e\\u7684\\u4e8b\\u4ef6\\u3002\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-07 22:32:40', '2020-12-07 22:32:40');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(67, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-07 22:32:41', '2020-12-07 22:32:41'),
(68, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-07 22:45:04', '2020-12-07 22:45:04');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(69, 1, 'admin/remedys/2', 'PUT', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"rcontent\":\"\\u53f0\\u7063\\u8d85\\u56b4\\u9694\\u96e2\\u6aa2\\u75ab\\u300c\\u51fa\\u623f8\\u79d2\\u906d\\u7f7010\\u842c\\u300d\\u767b\\u4e0aCNN\\r\\n113\\r\\n\\u83ef\\u8996\\r\\n\\u83ef\\u8996\\r\\n2020\\u5e7412\\u67088\\u65e5 \\u9031\\u4e8c \\u4e0a\\u534810:10 [GMT+8]\\u00b72 \\u5206\\u9418 (\\u95b1\\u8b80\\u6642\\u9593)\\r\\n\\r\\n\\u25b2 \\u6a5f\\u5834\\u6aa2\\u75ab\\u793a\\u610f\\u5716\\u3002\\uff08\\u8cc7\\u6599\\u7167\\u7247\\uff09\\r\\n\\r\\n\\/ \\u6731\\u57f9\\u59a4 \\u7d9c\\u5408\\u5831\\u5c0e\\r\\n\\r\\n\\u6b66\\u6f22\\u80ba\\u708e\\uff08COVID-19\\uff0c2019\\u65b0\\u578b\\u51a0\\u72c0\\u75c5\\u6bd2\\uff09\\u75ab\\u60c5\\u8086\\u8650\\u5168\\u7403\\uff0c\\u800c\\u53f0\\u7063\\u76f8\\u5c0d\\u60c5\\u6cc1\\u8da8\\u7de9\\uff0c\\u9632\\u75ab\\u6709\\u6210\\u6709\\u76ee\\u5171\\u7779\\uff1b\\u65e5\\u524d\\u53f0\\u7063\\u70ba\\u9632\\u5835\\u79fb\\u5de5\\u6210\\u70ba\\u9632\\u75ab\\u7834\\u53e3\\uff0c\\u9ad8\\u96c4\\u5e02\\u885b\\u751f\\u5c40\\u52a0\\u5f37\\u7a3d\\u67e5\\u9632\\u75ab\\u65c5\\u9928\\uff0c\\u7d50\\u679c\\u4e00\\u540d\\u83f2\\u7c4d\\u79fb\\u5de5\\u8d70\\u51fa\\u623f\\u9580\\u77ed\\u77ed8\\u79d2\\u9418\\u5c31\\u88ab\\u958b\\u7f7010\\u842c\\u5143\\u7f70\\u55ae\\uff0c\\u800c\\u53f0\\u7063\\u7684\\u300c\\u8d85\\u56b4\\u6aa2\\u75ab\\u300d\\u4e5f\\u767b\\u4e0a\\u7f8e\\u570bCNN\\u7db2\\u7ad9\\u982d\\u7248\\u3002\\r\\n\\r\\n\\u83f2\\u7c4d\\u79fb\\u5de5\\u6aa2\\u75ab\\u6e9c\\u51fa\\u623f8\\u79d2 \\u906d\\u885b\\u751f\\u5c40\\u7f70\\u6b3e10\\u842c\\r\\n\\u65e5\\u524d\\u570b\\u5167\\u5883\\u5916\\u79fb\\u5165\\u75c5\\u4f8b\\u589e\\u52a0\\uff0c\\u70ba\\u4e86\\u9632\\u5835\\u79fb\\u5de5\\u6210\\u70ba\\u9632\\u75ab\\u7834\\u53e3\\uff0c\\u9ad8\\u96c4\\u5e02\\u885b\\u751f\\u5c40\\u52a0\\u5f37\\u7a3d\\u67e5\\u9632\\u75ab\\u65c5\\u9928\\uff0c\\u5f9e11\\u67081\\u65e5\\u5230\\u76ee\\u524d\\u70ba\\u6b62\\uff0c\\u5171\\u67e5\\u523019\\u4eba\\u9055\\u898f\\u5916\\u51fa\\uff0c\\u5305\\u62ec\\u570b\\u969b\\u79fb\\u5de5\\u67099\\u540d\\uff0c\\u5176\\u4e2d\\uff0c\\u4e00\\u540d\\u83f2\\u5f8b\\u8cd3\\u7c4d\\u79fb\\u5de5\\u56e0\\u70ba\\u5077\\u6e9c\\u51fa\\u623f\\u9580\\u77ed\\u77ed8\\u79d2\\u9418\\uff0c\\u60f3\\u62ff\\u6771\\u897f\\u7d66\\u9694\\u58c1\\u623f\\u7684\\u670b\\u53cb\\uff0c\\u9055\\u898f\\u884c\\u70ba\\u5168\\u90fd\\u88ab\\u76e3\\u8996\\u5668\\u9304\\u4e0b\\uff0c\\u9ad8\\u96c4\\u5e02\\u885b\\u751f\\u5c40\\u4e5f\\u6c7a\\u5b9a\\u5c0d\\u8a72\\u540d\\u6f01\\u5de5\\u958b\\u51fa10\\u842c\\u5143\\u7f70\\u55ae\\u3002\\r\\n\\r\\n\\u800c\\u9019\\u6a23\\u7684\\u56b4\\u683c\\u6aa2\\u75ab\\u7f70\\u5247\\uff0c\\u4e5f\\u767b\\u4e0a\\u7f8e\\u570b\\u5a92\\u9ad4CNN\\u7db2\\u7ad9\\u7684\\u982d\\u7248\\uff0c\\u5831\\u5c0e\\u4ee5\\u300c\\u7537\\u5b50\\u6d89\\u5acc\\u7834\\u58de\\u53f0\\u7063\\u9694\\u96e2\\u6aa2\\u75ab8\\u79d2\\uff0c\\u906d\\u7f70\\u6b3e3500\\u7f8e\\u5143\\u300d\\u70ba\\u984c\\uff0c\\u63cf\\u8ff0\\u65e5\\u524d\\u9ad8\\u96c4\\u5e02\\u83f2\\u7c4d\\u79fb\\u5de5\\u88ab\\u7f70\\u6b3e\\u7684\\u4e8b\\u4ef6\\u3002\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-07 22:45:19', '2020-12-07 22:45:19');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(70, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-07 22:45:19', '2020-12-07 22:45:19'),
(71, 1, 'admin/remedys/2', 'PUT', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"rcontent\":\"\\\"\'\\\"\\\"\\\"\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_method\":\"PUT\"}', '2020-12-07 22:58:00', '2020-12-07 22:58:00'),
(72, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:58:00', '2020-12-07 22:58:00'),
(73, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:58:05', '2020-12-07 22:58:05'),
(74, 1, 'admin/remedys/2', 'PUT', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"rcontent\":\"\\\"\'\\\"\\\"\\\"cxcxcxc\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-07 22:58:08', '2020-12-07 22:58:08'),
(75, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:58:08', '2020-12-07 22:58:08'),
(76, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:58:10', '2020-12-07 22:58:10'),
(77, 1, 'admin/remedys/2', 'PUT', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"rcontent\":\"\\\"\'\\\"\\\"\\\"cxcxcxc\\r\\nxcxcxccc\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-07 22:58:13', '2020-12-07 22:58:13'),
(78, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:58:13', '2020-12-07 22:58:13'),
(79, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:58:15', '2020-12-07 22:58:15');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(80, 1, 'admin/remedys/2', 'PUT', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"rcontent\":\"\\\"\'\\\"\\\"\\\"cxcxcxc\\r\\nxcxcxccc\\r\\nYou take care of SQL injection.\\r\\n\\r\\nA framework may provide methods of doing that conveniently, but you still have to use the methods consistently.\\r\\n\\r\\nFor example, you should use query parameters instead of concatenating variables into your SQL expressions.\\r\\n\\r\\nRe your comment:\\r\\n\\r\\nEloquent has methods like whereRaw() which allow you to write any expression you want. Here\'s an example from the Eloquent docs:\\r\\n\\r\\n$users = User::whereRaw(\'age > ? and votes = 100\', [25])->get();\\r\\nIf you use this ? syntax for parameters, and pass the values as the array argument following, then yes, you can safely depend on Eloquent to use parameterization.\\r\\n\\r\\nBut it\'s not accurate to say \\\"Eloquent takes care of SQL injection\\\" because that leads some naive developers to think that you can do unsafe things like this:\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-07 22:58:29', '2020-12-07 22:58:29'),
(81, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:58:29', '2020-12-07 22:58:29'),
(82, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 22:58:32', '2020-12-07 22:58:32');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(83, 1, 'admin/remedys/2', 'PUT', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"rcontent\":\"\\\"\'\\\"\\\"\\\"cxcxcxc\\r\\nxcxcxccc\\r\\n\\u4f60\\u597d\\r\\nYou take care of SQL injection.\\r\\n\\r\\nA framework may provide methods of doing that conveniently, but you still have to use the methods consistently.\\r\\n\\r\\nFor example, you should use query parameters instead of concatenating variables into your SQL expressions.\\r\\n\\r\\nRe your comment:\\r\\n\\r\\nEloquent has methods like whereRaw() which allow you to write any expression you want. Here\'s an example from the Eloquent docs:\\r\\n\\r\\n$users = User::whereRaw(\'age > ? and votes = 100\', [25])->get();\\r\\nIf you use this ? syntax for parameters, and pass the values as the array argument following, then yes, you can safely depend on Eloquent to use parameterization.\\r\\n\\r\\nBut it\'s not accurate to say \\\"Eloquent takes care of SQL injection\\\" because that leads some naive developers to think that you can do unsafe things like this:\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-07 22:58:37', '2020-12-07 22:58:37'),
(84, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-07 22:58:37', '2020-12-07 22:58:37');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(85, 1, 'admin/remedys/2', 'PUT', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"rcontent\":\"\\\"\'\\\"\\\"\\\"cxcxcxc\\r\\nxcxcxccc\\r\\n\\u4f60\\u597d\\r\\nYou take care of SQL injection.\\r\\n\\r\\nA framework may provide methods of doing that conveniently, but you still have to use the methods consistently.\\r\\n\\r\\nFor example, you should use query parameters instead of concatenating variables into your SQL expressions.\\r\\n\\r\\nRe your comment:\\r\\n\\r\\nEloquent has methods like whereRaw() which allow you to write any expression you want. Here\'s an example from the Eloquent docs:\\r\\n\\r\\n$users = User::whereRaw(\'age > ? and votes = 100\', [25])->get();\\r\\nIf you use this ? syntax for parameters, and pass the values as the array argument following, then yes, you can safely depend on Eloquent to use parameterization.\\r\\n\\r\\nBut it\'s not accurate to say \\\"Eloquent takes care of SQL injection\\\" because that leads some naive developers to think that you can do unsafe things like this:\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_method\":\"PUT\"}', '2020-12-07 22:59:33', '2020-12-07 22:59:33'),
(86, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 22:59:33', '2020-12-07 22:59:33'),
(87, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 23:00:30', '2020-12-07 23:00:30'),
(88, 1, 'admin/remedys/1', 'PUT', '127.0.0.1', '{\"rname\":\"RRRRRRRRRRR\",\"rcontent\":\"CCCCCCCCCCCCCCCCCCC\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-07 23:00:38', '2020-12-07 23:00:38'),
(89, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:00:38', '2020-12-07 23:00:38'),
(90, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:01:15', '2020-12-07 23:01:15');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(91, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:01:37', '2020-12-07 23:01:37'),
(92, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:01:53', '2020-12-07 23:01:53'),
(93, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:02:31', '2020-12-07 23:02:31'),
(94, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:03:49', '2020-12-07 23:03:49'),
(95, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:04:04', '2020-12-07 23:04:04'),
(96, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:04:38', '2020-12-07 23:04:38'),
(97, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:04:49', '2020-12-07 23:04:49'),
(98, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:04:53', '2020-12-07 23:04:53'),
(99, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:05:00', '2020-12-07 23:05:00'),
(100, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:05:05', '2020-12-07 23:05:05'),
(101, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:05:44', '2020-12-07 23:05:44'),
(102, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:06:25', '2020-12-07 23:06:25'),
(103, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:06:45', '2020-12-07 23:06:45'),
(104, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:26:43', '2020-12-07 23:26:43'),
(105, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:26:54', '2020-12-07 23:26:54'),
(106, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:27:20', '2020-12-07 23:27:20'),
(107, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:32:01', '2020-12-07 23:32:01'),
(108, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:32:03', '2020-12-07 23:32:03'),
(109, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:32:11', '2020-12-07 23:32:11');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(110, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:32:24', '2020-12-07 23:32:24'),
(111, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:33:35', '2020-12-07 23:33:35'),
(112, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_columns_\":\"atime,auid,rcontent,rid,rname\",\"_pjax\":\"#pjax-container\"}', '2020-12-07 23:33:48', '2020-12-07 23:33:48'),
(113, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-07 23:33:51', '2020-12-07 23:33:51'),
(114, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:35:33', '2020-12-07 23:35:33'),
(115, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:36:36', '2020-12-07 23:36:36'),
(116, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:37:00', '2020-12-07 23:37:00'),
(117, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:37:15', '2020-12-07 23:37:15'),
(118, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:38:00', '2020-12-07 23:38:00'),
(119, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:38:42', '2020-12-07 23:38:42'),
(120, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:39:45', '2020-12-07 23:39:45'),
(121, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:40:10', '2020-12-07 23:40:10'),
(122, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:40:36', '2020-12-07 23:40:36'),
(123, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:46:37', '2020-12-07 23:46:37'),
(124, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:49:42', '2020-12-07 23:49:42'),
(125, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:53:03', '2020-12-07 23:53:03'),
(126, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:53:45', '2020-12-07 23:53:45'),
(127, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:55:18', '2020-12-07 23:55:18');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(128, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:55:58', '2020-12-07 23:55:58'),
(129, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:56:29', '2020-12-07 23:56:29'),
(130, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-07 23:57:13', '2020-12-07 23:57:13'),
(131, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:04:35', '2020-12-08 00:04:35'),
(132, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:05:37', '2020-12-08 00:05:37'),
(133, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:12:36', '2020-12-08 00:12:36'),
(134, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:14:08', '2020-12-08 00:14:08'),
(135, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:14:27', '2020-12-08 00:14:27'),
(136, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:14:38', '2020-12-08 00:14:38'),
(137, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:14:54', '2020-12-08 00:14:54'),
(138, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:16:58', '2020-12-08 00:16:58'),
(139, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:26:54', '2020-12-08 00:26:54'),
(140, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:27:30', '2020-12-08 00:27:30'),
(141, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:28:20', '2020-12-08 00:28:20'),
(142, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:30:44', '2020-12-08 00:30:44'),
(143, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:31:09', '2020-12-08 00:31:09'),
(144, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:36:08', '2020-12-08 00:36:08'),
(145, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:36:10', '2020-12-08 00:36:10'),
(146, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:36:49', '2020-12-08 00:36:49');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(147, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:37:10', '2020-12-08 00:37:10'),
(148, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:37:58', '2020-12-08 00:37:58'),
(149, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:38:14', '2020-12-08 00:38:14'),
(150, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:38:35', '2020-12-08 00:38:35'),
(151, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:39:08', '2020-12-08 00:39:08'),
(152, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:39:40', '2020-12-08 00:39:40'),
(153, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:39:54', '2020-12-08 00:39:54'),
(154, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:40:10', '2020-12-08 00:40:10'),
(155, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:40:47', '2020-12-08 00:40:47'),
(156, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:40:54', '2020-12-08 00:40:54'),
(157, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:43:12', '2020-12-08 00:43:12'),
(158, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:43:26', '2020-12-08 00:43:26'),
(159, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:43:46', '2020-12-08 00:43:46'),
(160, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 00:44:27', '2020-12-08 00:44:27'),
(161, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 00:44:59', '2020-12-08 00:44:59'),
(162, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:13:56', '2020-12-08 01:13:56');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(163, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":null,\"icon\":\"fa-bars\",\"uri\":\"remedys\",\"roles\":[null],\"permission\":null,\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\"}', '2020-12-08 01:14:06', '2020-12-08 01:14:06'),
(164, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-08 01:14:06', '2020-12-08 01:14:06'),
(165, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"remedys\",\"icon\":\"fa-bars\",\"uri\":\"remedys\",\"roles\":[null],\"permission\":null,\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\"}', '2020-12-08 01:14:10', '2020-12-08 01:14:10'),
(166, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-08 01:14:10', '2020-12-08 01:14:10'),
(167, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":8},{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]}]\"}', '2020-12-08 01:14:20', '2020-12-08 01:14:20'),
(168, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:14:20', '2020-12-08 01:14:20'),
(169, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:14:23', '2020-12-08 01:14:23'),
(170, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:14:27', '2020-12-08 01:14:27'),
(171, 1, 'admin/auth/menu/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:14:29', '2020-12-08 01:14:29');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(172, 1, 'admin/auth/menu/8', 'PUT', '127.0.0.1', '{\"parent_id\":\"2\",\"title\":\"remedys\",\"icon\":\"fa-bars\",\"uri\":\"remedys\",\"roles\":[null],\"permission\":\"*\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/menu\"}', '2020-12-08 01:14:34', '2020-12-08 01:14:34'),
(173, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-08 01:14:34', '2020-12-08 01:14:34'),
(174, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:14:36', '2020-12-08 01:14:36'),
(175, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:14:38', '2020-12-08 01:14:38'),
(176, 1, 'admin/auth/menu/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:14:41', '2020-12-08 01:14:41'),
(177, 1, 'admin/auth/menu/8', 'PUT', '127.0.0.1', '{\"parent_id\":\"2\",\"title\":\"remedys\",\"icon\":\"fa-bars\",\"uri\":\"remedys\",\"roles\":[null],\"permission\":\"auth.login\",\"_token\":\"DqU6ko1ktmmBhZS2kbwjDamy3yXJBHh9GeSyYFYY\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/menu\"}', '2020-12-08 01:14:48', '2020-12-08 01:14:48'),
(178, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-08 01:14:49', '2020-12-08 01:14:49'),
(179, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:14:50', '2020-12-08 01:14:50'),
(180, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:14:52', '2020-12-08 01:14:52'),
(181, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:14:54', '2020-12-08 01:14:54'),
(182, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:14:57', '2020-12-08 01:14:57');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(183, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:15:00', '2020-12-08 01:15:00'),
(184, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-12-08 01:15:09', '2020-12-08 01:15:09'),
(185, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:15:13', '2020-12-08 01:15:13'),
(186, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 01:21:33', '2020-12-08 01:21:33'),
(187, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:21:36', '2020-12-08 01:21:36'),
(188, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 01:21:38', '2020-12-08 01:21:38'),
(189, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 01:23:36', '2020-12-08 01:23:36'),
(190, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:23:38', '2020-12-08 01:23:38'),
(191, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 01:23:39', '2020-12-08 01:23:39'),
(192, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 01:24:57', '2020-12-08 01:24:57'),
(193, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:24:59', '2020-12-08 01:24:59'),
(194, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 01:24:59', '2020-12-08 01:24:59'),
(195, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 01:31:42', '2020-12-08 01:31:42'),
(196, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 01:31:44', '2020-12-08 01:31:44'),
(197, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-08 01:32:36', '2020-12-08 01:32:36'),
(198, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-08 01:33:37', '2020-12-08 01:33:37'),
(199, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-08 01:33:42', '2020-12-08 01:33:42');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(200, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-08 01:34:10', '2020-12-08 01:34:10'),
(201, 1, 'admin/remedys/2', 'PUT', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"rcontent\":\"\\\"\'\\\"\\\"\\\"cxcxcxc\\r\\nxcxcxccc\\r\\n\\u4f60\\u597d\\r\\nYou take care of SQL injection.\\r\\n\\r\\nA framework may provide methods of doing that conveniently, but you still have to use the methods consistently.\\r\\n\\r\\nFor example, you should use query parameters instead of concatenating variables into your SQL expressions.\\r\\n\\r\\nRe your comment:\\r\\n\\r\\nEloquent has methods like whereRaw() which allow you to write any expression you want. Here\'s an example from the Eloquent docs:\\r\\n\\r\\n$users = User::whereRaw(\'age > ? and votes = 100\', [25])->get();\\r\\nIf you use this ? syntax for parameters, and pass the values as the array argument following, then yes, you can safely depend on Eloquent to use parameterization.\\r\\n\\r\\nBut it\'s not accurate to say \\\"Eloquent takes care of SQL injection\\\" because that leads some naive developers to think that you can do unsafe things like this:\",\"cats\":[\"1\",null],\"_token\":\"1I0hfA00XY7cYJngR6qx157h1iJSX3hVp6nsxocT\",\"_method\":\"PUT\"}', '2020-12-08 01:34:22', '2020-12-08 01:34:22'),
(202, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-08 01:34:23', '2020-12-08 01:34:23'),
(203, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-08 01:35:27', '2020-12-08 01:35:27');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(204, 1, 'admin/remedys/2', 'PUT', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"rcontent\":\"\\\"\'\\\"\\\"\\\"cxcxcxc\\r\\nxcxcxccc\\r\\n\\u4f60\\u597d\\r\\nYou take care of SQL injection.\\r\\n\\r\\nA framework may provide methods of doing that conveniently, but you still have to use the methods consistently.\\r\\n\\r\\nFor example, you should use query parameters instead of concatenating variables into your SQL expressions.\\r\\n\\r\\nRe your comment:\\r\\n\\r\\nEloquent has methods like whereRaw() which allow you to write any expression you want. Here\'s an example from the Eloquent docs:\\r\\n\\r\\n$users = User::whereRaw(\'age > ? and votes = 100\', [25])->get();\\r\\nIf you use this ? syntax for parameters, and pass the values as the array argument following, then yes, you can safely depend on Eloquent to use parameterization.\\r\\n\\r\\nBut it\'s not accurate to say \\\"Eloquent takes care of SQL injection\\\" because that leads some naive developers to think that you can do unsafe things like this:\",\"cats\":[\"1\",\"2\",null],\"_token\":\"1I0hfA00XY7cYJngR6qx157h1iJSX3hVp6nsxocT\",\"_method\":\"PUT\"}', '2020-12-08 01:35:31', '2020-12-08 01:35:31'),
(205, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '[]', '2020-12-08 01:35:31', '2020-12-08 01:35:31'),
(206, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-08 17:45:58', '2020-12-08 17:45:58'),
(207, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 17:46:01', '2020-12-08 17:46:01'),
(208, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-08 17:55:30', '2020-12-08 17:55:30'),
(209, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 17:55:32', '2020-12-08 17:55:32'),
(210, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 17:55:35', '2020-12-08 17:55:35');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(211, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 17:55:36', '2020-12-08 17:55:36'),
(212, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 17:58:29', '2020-12-08 17:58:29'),
(213, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 17:58:31', '2020-12-08 17:58:31'),
(214, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 17:58:32', '2020-12-08 17:58:32'),
(215, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 17:58:57', '2020-12-08 17:58:57'),
(216, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 17:58:59', '2020-12-08 17:58:59'),
(217, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 17:59:00', '2020-12-08 17:59:00'),
(218, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 17:59:10', '2020-12-08 17:59:10'),
(219, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 17:59:11', '2020-12-08 17:59:11'),
(220, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 17:59:44', '2020-12-08 17:59:44'),
(221, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 17:59:46', '2020-12-08 17:59:46'),
(222, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 17:59:47', '2020-12-08 17:59:47'),
(223, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 17:59:56', '2020-12-08 17:59:56'),
(224, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 17:59:58', '2020-12-08 17:59:58'),
(225, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 17:59:59', '2020-12-08 17:59:59'),
(226, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:00:22', '2020-12-08 18:00:22'),
(227, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:00:24', '2020-12-08 18:00:24');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(228, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:00:25', '2020-12-08 18:00:25'),
(229, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:01:06', '2020-12-08 18:01:06'),
(230, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:01:08', '2020-12-08 18:01:08'),
(231, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:01:08', '2020-12-08 18:01:08'),
(232, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:02:20', '2020-12-08 18:02:20'),
(233, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:02:22', '2020-12-08 18:02:22'),
(234, 1, 'admin/remedys/1', 'PUT', '127.0.0.1', '{\"rname\":\"RRRRRRRRRRR\",\"rcontent\":\"CCCCCCCCCCCCCCCCCCC\",\"cats\":[\"4\",\"3\",null],\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-08 18:02:31', '2020-12-08 18:02:31'),
(235, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:02:31', '2020-12-08 18:02:31'),
(236, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:02:34', '2020-12-08 18:02:34'),
(237, 1, 'admin/remedys/1', 'PUT', '127.0.0.1', '{\"rname\":\"RRRRRRRRRRR\",\"rcontent\":\"CCCCCCCCCCCCCCCCCCC\",\"cats\":[\"4\",null],\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-08 18:02:42', '2020-12-08 18:02:42'),
(238, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:02:42', '2020-12-08 18:02:42'),
(239, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:10:07', '2020-12-08 18:10:07'),
(240, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:10:54', '2020-12-08 18:10:54');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(241, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:12:44', '2020-12-08 18:12:44'),
(242, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:13:11', '2020-12-08 18:13:11'),
(243, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:20:57', '2020-12-08 18:20:57'),
(244, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:25:26', '2020-12-08 18:25:26'),
(245, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:26:33', '2020-12-08 18:26:33'),
(246, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:27:04', '2020-12-08 18:27:04'),
(247, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:27:28', '2020-12-08 18:27:28'),
(248, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:27:33', '2020-12-08 18:27:33');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(249, 1, 'admin/remedys/2', 'PUT', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"rcontent\":\"\\\"\'\\\"\\\"\\\"cxcxcxc\\r\\nxcxcxccc\\r\\n\\u4f60\\u597d\\r\\nYou take care of SQL injection.\\r\\n\\r\\nA framework may provide methods of doing that conveniently, but you still have to use the methods consistently.\\r\\n\\r\\nFor example, you should use query parameters instead of concatenating variables into your SQL expressions.\\r\\n\\r\\nRe your comment:\\r\\n\\r\\nEloquent has methods like whereRaw() which allow you to write any expression you want. Here\'s an example from the Eloquent docs:\\r\\n\\r\\n$users = User::whereRaw(\'age > ? and votes = 100\', [25])->get();\\r\\nIf you use this ? syntax for parameters, and pass the values as the array argument following, then yes, you can safely depend on Eloquent to use parameterization.\\r\\n\\r\\nBut it\'s not accurate to say \\\"Eloquent takes care of SQL injection\\\" because that leads some naive developers to think that you can do unsafe things like this:\",\"cats\":[\"3\",null],\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-08 18:27:35', '2020-12-08 18:27:35'),
(250, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:27:36', '2020-12-08 18:27:36'),
(251, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:32:28', '2020-12-08 18:32:28'),
(252, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 18:33:25', '2020-12-08 18:33:25'),
(253, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:33:33', '2020-12-08 18:33:33'),
(254, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\"}', '2020-12-08 18:34:29', '2020-12-08 18:34:29');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(255, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\"}', '2020-12-08 18:34:38', '2020-12-08 18:34:38'),
(256, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\"}', '2020-12-08 18:35:41', '2020-12-08 18:35:41'),
(257, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"desc\"},\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:35:43', '2020-12-08 18:35:43'),
(258, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:35:44', '2020-12-08 18:35:44'),
(259, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"}}', '2020-12-08 18:37:09', '2020-12-08 18:37:09'),
(260, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":\"\\u4f60\",\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:37:16', '2020-12-08 18:37:16'),
(261, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":\"\\u4f60\"}', '2020-12-08 18:38:07', '2020-12-08 18:38:07'),
(262, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:38:08', '2020-12-08 18:38:08'),
(263, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"_pjax\":\"#pjax-container\",\"rid\":null,\"rname\":\"\\u4f60\"}', '2020-12-08 18:38:12', '2020-12-08 18:38:12'),
(264, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:38:15', '2020-12-08 18:38:15');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(265, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"_pjax\":\"#pjax-container\",\"rid\":null,\"rname\":\"5\"}', '2020-12-08 18:38:18', '2020-12-08 18:38:18'),
(266, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:38:20', '2020-12-08 18:38:20'),
(267, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"}}', '2020-12-08 18:39:35', '2020-12-08 18:39:35'),
(268, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"}}', '2020-12-08 18:40:34', '2020-12-08 18:40:34'),
(269, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"}}', '2020-12-08 18:41:03', '2020-12-08 18:41:03'),
(270, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"}}', '2020-12-08 18:44:04', '2020-12-08 18:44:04'),
(271, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"}}', '2020-12-08 18:51:05', '2020-12-08 18:51:05'),
(272, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"}}', '2020-12-08 18:52:36', '2020-12-08 18:52:36'),
(273, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"}}', '2020-12-08 18:52:57', '2020-12-08 18:52:57'),
(274, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"}}', '2020-12-08 18:54:41', '2020-12-08 18:54:41');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(275, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"m\",\"f\"]},\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:54:46', '2020-12-08 18:54:46'),
(276, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"m\",\"f\"]}}', '2020-12-08 18:55:45', '2020-12-08 18:55:45'),
(277, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"m\",\"f\"]}}', '2020-12-08 18:57:23', '2020-12-08 18:57:23'),
(278, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"m\",\"f\"]}}', '2020-12-08 18:58:13', '2020-12-08 18:58:13'),
(279, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"3\",\"4\"]},\"_pjax\":\"#pjax-container\"}', '2020-12-08 18:58:15', '2020-12-08 18:58:15'),
(280, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"_pjax\":\"#pjax-container\",\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"3\"]}}', '2020-12-08 18:58:17', '2020-12-08 18:58:17'),
(281, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"_pjax\":\"#pjax-container\",\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"3\"]}}', '2020-12-08 19:00:11', '2020-12-08 19:00:11');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(282, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"3\"]}}', '2020-12-08 19:00:25', '2020-12-08 19:00:25'),
(283, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"3\"]}}', '2020-12-08 19:05:33', '2020-12-08 19:05:33'),
(284, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"3\"]},\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:05:35', '2020-12-08 19:05:35'),
(285, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"3\"]}}', '2020-12-08 19:05:40', '2020-12-08 19:05:40'),
(286, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"3\"]}}', '2020-12-08 19:06:14', '2020-12-08 19:06:14'),
(287, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"3\"]},\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:06:16', '2020-12-08 19:06:16'),
(288, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"3\"]}}', '2020-12-08 19:06:21', '2020-12-08 19:06:21');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(289, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"3\"]}}', '2020-12-08 19:06:50', '2020-12-08 19:06:50'),
(290, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"rid\":null,\"rname\":null,\"r_cats_remedys\":{\"remedys_rid\":[\"3\"]}}', '2020-12-08 19:07:21', '2020-12-08 19:07:21'),
(291, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"]},\"rid\":null,\"rname\":null,\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:07:25', '2020-12-08 19:07:25'),
(292, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"]},\"rid\":null,\"rname\":null}', '2020-12-08 19:07:33', '2020-12-08 19:07:33'),
(293, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"]},\"rid\":null,\"rname\":null}', '2020-12-08 19:08:16', '2020-12-08 19:08:16'),
(294, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"],\"r_cats_cid\":[\"3\"]},\"rid\":null,\"rname\":null,\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:08:19', '2020-12-08 19:08:19');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(295, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"],\"r_cats_cid\":[\"3\",\"4\"]},\"_pjax\":\"#pjax-container\",\"rid\":null,\"rname\":null}', '2020-12-08 19:08:21', '2020-12-08 19:08:21'),
(296, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"],\"r_cats_cid\":[\"4\"]},\"_pjax\":\"#pjax-container\",\"rid\":null,\"rname\":null}', '2020-12-08 19:08:23', '2020-12-08 19:08:23'),
(297, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"],\"r_cats_cid\":[\"4\"]},\"rid\":null,\"rname\":null}', '2020-12-08 19:09:11', '2020-12-08 19:09:11'),
(298, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"],\"r_cats_cid\":[\"4\"]},\"rid\":null,\"rname\":null}', '2020-12-08 19:09:21', '2020-12-08 19:09:21'),
(299, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"],\"r_cats_cid\":[\"4\"]},\"rid\":null,\"rname\":null}', '2020-12-08 19:09:33', '2020-12-08 19:09:33'),
(300, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"],\"r_cats_cid\":[\"4\"]},\"rid\":null,\"rname\":null}', '2020-12-08 19:11:28', '2020-12-08 19:11:28');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(301, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"],\"r_cats_cid\":[\"4\"]},\"rid\":null,\"rname\":null}', '2020-12-08 19:11:49', '2020-12-08 19:11:49'),
(302, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"]},\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:11:53', '2020-12-08 19:11:53'),
(303, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"],\"r_cats_cid\":[\"3\"]},\"_pjax\":\"#pjax-container\",\"rid\":null,\"rname\":null}', '2020-12-08 19:11:57', '2020-12-08 19:11:57'),
(304, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"cats\":\"AA\",\"_sort\":{\"column\":\"rname\",\"type\":\"asc\"},\"r_cats_remedys\":{\"remedys_rid\":[\"3\"],\"remedys_cid\":[\"3\"]},\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:12:01', '2020-12-08 19:12:01'),
(305, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:12:39', '2020-12-08 19:12:39'),
(306, 1, 'admin/rcats', 'GET', '127.0.0.1', '[]', '2020-12-08 19:13:17', '2020-12-08 19:13:17'),
(307, 1, 'admin/rcats', 'GET', '127.0.0.1', '[]', '2020-12-08 19:13:48', '2020-12-08 19:13:48'),
(308, 1, 'admin/rcats', 'GET', '127.0.0.1', '[]', '2020-12-08 19:14:13', '2020-12-08 19:14:13'),
(309, 1, 'admin/rcats/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:14:16', '2020-12-08 19:14:16'),
(310, 1, 'admin/rcats', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:14:22', '2020-12-08 19:14:22'),
(311, 1, 'admin/rcats', 'GET', '127.0.0.1', '[]', '2020-12-08 19:15:01', '2020-12-08 19:15:01');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(312, 1, 'admin/rcats', 'GET', '127.0.0.1', '[]', '2020-12-08 19:15:11', '2020-12-08 19:15:11'),
(313, 1, 'admin/rcats', 'GET', '127.0.0.1', '[]', '2020-12-08 19:15:18', '2020-12-08 19:15:18'),
(314, 1, 'admin/rcats/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:15:28', '2020-12-08 19:15:28'),
(315, 1, 'admin/rcats/4', 'PUT', '127.0.0.1', '{\"cname\":\"EEE\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/rcats\"}', '2020-12-08 19:15:34', '2020-12-08 19:15:34'),
(316, 1, 'admin/rcats', 'GET', '127.0.0.1', '[]', '2020-12-08 19:15:34', '2020-12-08 19:15:34'),
(317, 1, 'admin/rcats/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:15:39', '2020-12-08 19:15:39'),
(318, 1, 'admin/rcats', 'POST', '127.0.0.1', '{\"cname\":\"QQQ\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/rcats\"}', '2020-12-08 19:15:42', '2020-12-08 19:15:42'),
(319, 1, 'admin/rcats', 'GET', '127.0.0.1', '[]', '2020-12-08 19:15:43', '2020-12-08 19:15:43'),
(320, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:15:53', '2020-12-08 19:15:53'),
(321, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u5206\\u985e\",\"icon\":\"fa-bars\",\"uri\":\"rcats\",\"roles\":[null],\"permission\":\"auth.login\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\"}', '2020-12-08 19:16:13', '2020-12-08 19:16:13'),
(322, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-08 19:16:13', '2020-12-08 19:16:13');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(323, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":8},{\\\"id\\\":9},{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]}]\"}', '2020-12-08 19:16:19', '2020-12-08 19:16:19'),
(324, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:16:19', '2020-12-08 19:16:19'),
(325, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:16:20', '2020-12-08 19:16:20'),
(326, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:16:24', '2020-12-08 19:16:24'),
(327, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:16:26', '2020-12-08 19:16:26'),
(328, 1, 'admin/rcats', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:16:28', '2020-12-08 19:16:28'),
(329, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:16:30', '2020-12-08 19:16:30'),
(330, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:19:16', '2020-12-08 19:19:16'),
(331, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:19:29', '2020-12-08 19:19:29'),
(332, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:19:42', '2020-12-08 19:19:42'),
(333, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:23:51', '2020-12-08 19:23:51'),
(334, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:24:28', '2020-12-08 19:24:28'),
(335, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:24:44', '2020-12-08 19:24:44'),
(336, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:25:07', '2020-12-08 19:25:07'),
(337, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:25:23', '2020-12-08 19:25:23');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(338, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:26:56', '2020-12-08 19:26:56'),
(339, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:27:25', '2020-12-08 19:27:25'),
(340, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:27:37', '2020-12-08 19:27:37'),
(341, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:27:59', '2020-12-08 19:27:59'),
(342, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:28:02', '2020-12-08 19:28:02'),
(343, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:28:11', '2020-12-08 19:28:11'),
(344, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:28:21', '2020-12-08 19:28:21'),
(345, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:28:54', '2020-12-08 19:28:54'),
(346, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:29:59', '2020-12-08 19:29:59'),
(347, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:30:23', '2020-12-08 19:30:23'),
(348, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:35:11', '2020-12-08 19:35:11'),
(349, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:35:12', '2020-12-08 19:35:12'),
(350, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:35:45', '2020-12-08 19:35:45'),
(351, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:36:05', '2020-12-08 19:36:05'),
(352, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:36:18', '2020-12-08 19:36:18'),
(353, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:36:29', '2020-12-08 19:36:29'),
(354, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:39:52', '2020-12-08 19:39:52'),
(355, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:40:00', '2020-12-08 19:40:00'),
(356, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:44:15', '2020-12-08 19:44:15');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(357, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:44:32', '2020-12-08 19:44:32'),
(358, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:45:10', '2020-12-08 19:45:10'),
(359, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:45:22', '2020-12-08 19:45:22'),
(360, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:45:59', '2020-12-08 19:45:59'),
(361, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:46:11', '2020-12-08 19:46:11'),
(362, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:46:33', '2020-12-08 19:46:33'),
(363, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:47:28', '2020-12-08 19:47:28'),
(364, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:49:55', '2020-12-08 19:49:55'),
(365, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:50:15', '2020-12-08 19:50:15'),
(366, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:50:23', '2020-12-08 19:50:23'),
(367, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:50:35', '2020-12-08 19:50:35'),
(368, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:50:47', '2020-12-08 19:50:47'),
(369, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:51:05', '2020-12-08 19:51:05'),
(370, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:51:19', '2020-12-08 19:51:19'),
(371, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:51:21', '2020-12-08 19:51:21'),
(372, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 19:52:21', '2020-12-08 19:52:21'),
(373, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 19:52:43', '2020-12-08 19:52:43'),
(374, 1, 'admin/rfuns', 'GET', '127.0.0.1', '[]', '2020-12-08 20:01:06', '2020-12-08 20:01:06'),
(375, 1, 'admin/rfuns', 'GET', '127.0.0.1', '[]', '2020-12-08 20:01:27', '2020-12-08 20:01:27');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(376, 1, 'admin/rfuns', 'GET', '127.0.0.1', '[]', '2020-12-08 20:01:29', '2020-12-08 20:01:29'),
(377, 1, 'admin/rfuns', 'GET', '127.0.0.1', '[]', '2020-12-08 20:02:12', '2020-12-08 20:02:12'),
(378, 1, 'admin/rfuns', 'GET', '127.0.0.1', '[]', '2020-12-08 20:02:14', '2020-12-08 20:02:14'),
(379, 1, 'admin/rfuns', 'GET', '127.0.0.1', '[]', '2020-12-08 20:02:50', '2020-12-08 20:02:50'),
(380, 1, 'admin/rfuns/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 20:02:52', '2020-12-08 20:02:52'),
(381, 1, 'admin/rfuns', 'POST', '127.0.0.1', '{\"fname\":\"\\u529f\\u80fd1\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/rfuns\"}', '2020-12-08 20:03:01', '2020-12-08 20:03:01'),
(382, 1, 'admin/rfuns', 'GET', '127.0.0.1', '[]', '2020-12-08 20:03:01', '2020-12-08 20:03:01'),
(383, 1, 'admin/rfuns/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 20:03:02', '2020-12-08 20:03:02'),
(384, 1, 'admin/rfuns', 'POST', '127.0.0.1', '{\"fname\":\"\\u529f\\u80fd2\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/rfuns\"}', '2020-12-08 20:03:05', '2020-12-08 20:03:05'),
(385, 1, 'admin/rfuns', 'GET', '127.0.0.1', '[]', '2020-12-08 20:03:05', '2020-12-08 20:03:05'),
(386, 1, 'admin/rfuns/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 20:03:07', '2020-12-08 20:03:07'),
(387, 1, 'admin/rfuns', 'POST', '127.0.0.1', '{\"fname\":\"\\u529f\\u80fd3\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/rfuns\"}', '2020-12-08 20:03:09', '2020-12-08 20:03:09'),
(388, 1, 'admin/rfuns', 'GET', '127.0.0.1', '[]', '2020-12-08 20:03:10', '2020-12-08 20:03:10');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(389, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 20:03:12', '2020-12-08 20:03:12'),
(390, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 20:03:15', '2020-12-08 20:03:15'),
(391, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:03:16', '2020-12-08 20:03:16'),
(392, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:03:42', '2020-12-08 20:03:42'),
(393, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 20:03:44', '2020-12-08 20:03:44'),
(394, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:03:44', '2020-12-08 20:03:44'),
(395, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:04:24', '2020-12-08 20:04:24'),
(396, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 20:04:27', '2020-12-08 20:04:27'),
(397, 1, 'admin/remedys/1', 'PUT', '127.0.0.1', '{\"rname\":\"RRRRRRRRRRR\",\"cats\":[\"4\",null],\"funs\":[\"1\",\"2\",null],\"rcontent\":\"CCCCCCCCCCCCCCCCCCC\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-08 20:04:32', '2020-12-08 20:04:32'),
(398, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:04:32', '2020-12-08 20:04:32'),
(399, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:05:18', '2020-12-08 20:05:18'),
(400, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:05:27', '2020-12-08 20:05:27'),
(401, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:05:53', '2020-12-08 20:05:53'),
(402, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 20:05:56', '2020-12-08 20:05:56');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(403, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 20:05:58', '2020-12-08 20:05:58'),
(404, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:06:39', '2020-12-08 20:06:39'),
(405, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:06:40', '2020-12-08 20:06:40'),
(406, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:06:53', '2020-12-08 20:06:53'),
(407, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:07:18', '2020-12-08 20:07:18'),
(408, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:07:32', '2020-12-08 20:07:32'),
(409, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 20:08:08', '2020-12-08 20:08:08'),
(410, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 21:43:29', '2020-12-08 21:43:29'),
(411, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 21:45:09', '2020-12-08 21:45:09'),
(412, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 21:46:16', '2020-12-08 21:46:16'),
(413, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 21:46:27', '2020-12-08 21:46:27'),
(414, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"rid\":null,\"rname\":null,\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"]},\"_pjax\":\"#pjax-container\"}', '2020-12-08 21:46:31', '2020-12-08 21:46:31'),
(415, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"rid\":null,\"rname\":null,\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"]}}', '2020-12-08 21:47:09', '2020-12-08 21:47:09'),
(416, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"rid\":null,\"rname\":null,\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"]}}', '2020-12-08 21:49:01', '2020-12-08 21:49:01'),
(417, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"],\"r_funs_fid\":[\"1\"]},\"rid\":null,\"rname\":null,\"_pjax\":\"#pjax-container\"}', '2020-12-08 21:49:05', '2020-12-08 21:49:05');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(418, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"],\"r_funs_fid\":[\"1\"]},\"rid\":null,\"rname\":null}', '2020-12-08 21:50:17', '2020-12-08 21:50:17'),
(419, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"],\"r_funs_fid\":[\"1\"]},\"rid\":null,\"rname\":null}', '2020-12-08 21:52:09', '2020-12-08 21:52:09'),
(420, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"],\"r_funs_fid\":[\"1\",\"2\"]},\"rid\":null,\"rname\":null,\"_pjax\":\"#pjax-container\"}', '2020-12-08 21:52:12', '2020-12-08 21:52:12'),
(421, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"],\"r_funs_fid\":[\"2\"]},\"_pjax\":\"#pjax-container\",\"rid\":null,\"rname\":null}', '2020-12-08 21:52:13', '2020-12-08 21:52:13'),
(422, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"],\"r_funs_fid\":[\"1\",\"2\"]},\"_pjax\":\"#pjax-container\",\"rid\":null,\"rname\":null}', '2020-12-08 21:52:17', '2020-12-08 21:52:17'),
(423, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"]},\"_pjax\":\"#pjax-container\",\"rid\":null,\"rname\":null}', '2020-12-08 21:52:21', '2020-12-08 21:52:21'),
(424, 1, 'admin/remedys/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 21:52:25', '2020-12-08 21:52:25');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(425, 1, 'admin/remedys/2', 'PUT', '127.0.0.1', '{\"rname\":\"5tyrfhgfhbfhb\",\"cats\":[\"3\",null],\"funs\":[\"1\",\"3\",null],\"rcontent\":\"\\\"\'\\\"\\\"\\\"cxcxcxc\\r\\nxcxcxccc\\r\\n\\u4f60\\u597d\\r\\nYou take care of SQL injection.\\r\\n\\r\\nA framework may provide methods of doing that conveniently, but you still have to use the methods consistently.\\r\\n\\r\\nFor example, you should use query parameters instead of concatenating variables into your SQL expressions.\\r\\n\\r\\nRe your comment:\\r\\n\\r\\nEloquent has methods like whereRaw() which allow you to write any expression you want. Here\'s an example from the Eloquent docs:\\r\\n\\r\\n$users = User::whereRaw(\'age > ? and votes = 100\', [25])->get();\\r\\nIf you use this ? syntax for parameters, and pass the values as the array argument following, then yes, you can safely depend on Eloquent to use parameterization.\\r\\n\\r\\nBut it\'s not accurate to say \\\"Eloquent takes care of SQL injection\\\" because that leads some naive developers to think that you can do unsafe things like this:\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys?r_funs_remedys%5Br_funs_cid%5D%5B0%5D=1&rid=&rname=\"}', '2020-12-08 21:52:30', '2020-12-08 21:52:30'),
(426, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"]},\"rid\":null,\"rname\":null}', '2020-12-08 21:52:31', '2020-12-08 21:52:31'),
(427, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"],\"r_funs_fid\":[\"2\",\"3\"]},\"rid\":null,\"rname\":null,\"_pjax\":\"#pjax-container\"}', '2020-12-08 21:52:38', '2020-12-08 21:52:38');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(428, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"],\"r_funs_fid\":[\"3\"]},\"_pjax\":\"#pjax-container\",\"rid\":null,\"rname\":null}', '2020-12-08 21:52:40', '2020-12-08 21:52:40'),
(429, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"]},\"_pjax\":\"#pjax-container\"}', '2020-12-08 21:53:00', '2020-12-08 21:53:00'),
(430, 1, 'admin/remedys/1', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 21:53:27', '2020-12-08 21:53:27'),
(431, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"]},\"_pjax\":\"#pjax-container\"}', '2020-12-08 21:53:30', '2020-12-08 21:53:30'),
(432, 1, 'admin/remedys/2', 'DELETE', '127.0.0.1', '{\"_method\":\"delete\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\"}', '2020-12-08 21:55:38', '2020-12-08 21:55:38'),
(433, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"]},\"_pjax\":\"#pjax-container\"}', '2020-12-08 21:55:38', '2020-12-08 21:55:38'),
(434, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 21:55:45', '2020-12-08 21:55:45'),
(435, 1, 'admin/remedys', 'POST', '127.0.0.1', '{\"rname\":\"\\u4f60\",\"cats\":[\"4\",null],\"funs\":[\"2\",null],\"rcontent\":\"fdfgdfgfdg\\r\\ngfdgdfgdfgd\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys?r_funs_remedys%5Br_funs_cid%5D%5B0%5D=1\"}', '2020-12-08 21:55:53', '2020-12-08 21:55:53'),
(436, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"]}}', '2020-12-08 21:55:53', '2020-12-08 21:55:53'),
(437, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"]}}', '2020-12-08 22:02:47', '2020-12-08 22:02:47');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(438, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"r_funs_remedys\":{\"r_funs_cid\":[\"1\"]}}', '2020-12-08 22:08:54', '2020-12-08 22:08:54'),
(439, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 22:51:23', '2020-12-08 22:51:23'),
(440, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u7d20\\u6750\",\"icon\":\"fa-bars\",\"uri\":\"rmaterials\",\"roles\":[null],\"permission\":\"auth.login\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\"}', '2020-12-08 22:51:56', '2020-12-08 22:51:56'),
(441, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-08 22:51:56', '2020-12-08 22:51:56'),
(442, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u529f\\u6548\",\"icon\":\"fa-bars\",\"uri\":\"rfuns\",\"roles\":[null],\"permission\":\"auth.login\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\"}', '2020-12-08 22:52:24', '2020-12-08 22:52:24'),
(443, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-08 22:52:24', '2020-12-08 22:52:24'),
(444, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":8},{\\\"id\\\":9},{\\\"id\\\":11},{\\\"id\\\":10},{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]}]\"}', '2020-12-08 22:52:36', '2020-12-08 22:52:36'),
(445, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 22:52:36', '2020-12-08 22:52:36'),
(446, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-08 22:52:39', '2020-12-08 22:52:39'),
(447, 1, 'admin/rcats', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 22:52:41', '2020-12-08 22:52:41');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(448, 1, 'admin/rfuns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 22:52:42', '2020-12-08 22:52:42'),
(449, 1, 'admin/rmaterials', 'GET', '127.0.0.1', '[]', '2020-12-08 22:53:03', '2020-12-08 22:53:03'),
(450, 1, 'admin/rmaterials', 'GET', '127.0.0.1', '[]', '2020-12-08 22:53:13', '2020-12-08 22:53:13'),
(451, 1, 'admin/rmaterials/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 22:53:15', '2020-12-08 22:53:15'),
(452, 1, 'admin/rmaterials', 'POST', '127.0.0.1', '{\"mname\":\"\\u7576\\u6b78\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/rmaterials\"}', '2020-12-08 22:53:20', '2020-12-08 22:53:20'),
(453, 1, 'admin/rmaterials', 'GET', '127.0.0.1', '[]', '2020-12-08 22:53:21', '2020-12-08 22:53:21'),
(454, 1, 'admin/rmaterials/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 22:53:22', '2020-12-08 22:53:22'),
(455, 1, 'admin/rmaterials', 'POST', '127.0.0.1', '{\"mname\":\"\\u67b8\\u675e\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/rmaterials\"}', '2020-12-08 22:53:28', '2020-12-08 22:53:28'),
(456, 1, 'admin/rmaterials', 'GET', '127.0.0.1', '[]', '2020-12-08 22:53:28', '2020-12-08 22:53:28'),
(457, 1, 'admin/rmaterials/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 22:53:29', '2020-12-08 22:53:29'),
(458, 1, 'admin/rmaterials', 'POST', '127.0.0.1', '{\"mname\":\"\\u827e\\u8349\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/rmaterials\"}', '2020-12-08 22:53:40', '2020-12-08 22:53:40'),
(459, 1, 'admin/rmaterials', 'GET', '127.0.0.1', '[]', '2020-12-08 22:53:40', '2020-12-08 22:53:40');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(460, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 22:53:44', '2020-12-08 22:53:44'),
(461, 1, 'admin/rmaterials', 'GET', '127.0.0.1', '[]', '2020-12-08 22:53:46', '2020-12-08 22:53:46'),
(462, 1, 'admin/rmaterials', 'GET', '127.0.0.1', '[]', '2020-12-08 22:54:41', '2020-12-08 22:54:41'),
(463, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 22:54:45', '2020-12-08 22:54:45'),
(464, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 22:55:35', '2020-12-08 22:55:35'),
(465, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 22:55:39', '2020-12-08 22:55:39'),
(466, 1, 'admin/rmaterials', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 22:55:43', '2020-12-08 22:55:43'),
(467, 1, 'admin/rmaterials', 'GET', '127.0.0.1', '[]', '2020-12-08 22:56:18', '2020-12-08 22:56:18'),
(468, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 22:56:20', '2020-12-08 22:56:20'),
(469, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 22:56:23', '2020-12-08 22:56:23'),
(470, 1, 'admin/remedys', 'POST', '127.0.0.1', '{\"rname\":\"WWWW\",\"cats\":[\"3\",null],\"funs\":[\"3\",null],\"materials\":[\"2\",\"3\",null],\"rcontent\":\"cxcxcxc\\r\\nxcxcx\\r\\ncxcxccxcxc\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-08 22:56:36', '2020-12-08 22:56:36'),
(471, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 22:56:36', '2020-12-08 22:56:36'),
(472, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 22:57:30', '2020-12-08 22:57:30'),
(473, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 22:59:06', '2020-12-08 22:59:06');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(474, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:00:46', '2020-12-08 23:00:46'),
(475, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:01:20', '2020-12-08 23:01:20'),
(476, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:02:06', '2020-12-08 23:02:06'),
(477, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:03:46', '2020-12-08 23:03:46'),
(478, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"rid\":null,\"rname\":null,\"r_materials_remedys\":{\"r_materials_mid\":[\"3\"]},\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:04:12', '2020-12-08 23:04:12'),
(479, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"rid\":null,\"rname\":null,\"r_materials_remedys\":{\"r_materials_mid\":[\"3\"]}}', '2020-12-08 23:04:40', '2020-12-08 23:04:40'),
(480, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:04:44', '2020-12-08 23:04:44'),
(481, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:04:58', '2020-12-08 23:04:58'),
(482, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:09:20', '2020-12-08 23:09:20'),
(483, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:09:23', '2020-12-08 23:09:23'),
(484, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:09:26', '2020-12-08 23:09:26'),
(485, 1, 'admin/auth/menu/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:09:29', '2020-12-08 23:09:29');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(486, 1, 'admin/auth/menu/8', 'PUT', '127.0.0.1', '{\"parent_id\":\"2\",\"title\":\"\\u504f\\u65b9\",\"icon\":\"fa-bars\",\"uri\":\"remedys\",\"roles\":[null],\"permission\":\"auth.login\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/menu\"}', '2020-12-08 23:09:40', '2020-12-08 23:09:40'),
(487, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-08 23:09:40', '2020-12-08 23:09:40'),
(488, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:09:43', '2020-12-08 23:09:43'),
(489, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:09:45', '2020-12-08 23:09:45'),
(490, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:10:02', '2020-12-08 23:10:02'),
(491, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:10:26', '2020-12-08 23:10:26'),
(492, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:10:54', '2020-12-08 23:10:54'),
(493, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:11:01', '2020-12-08 23:11:01'),
(494, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:11:36', '2020-12-08 23:11:36'),
(495, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '[]', '2020-12-08 23:12:06', '2020-12-08 23:12:06'),
(496, 1, 'admin/remedys/1', 'PUT', '127.0.0.1', '{\"rname\":\"RRRRRRRRRRR\",\"cats\":[\"4\",null],\"funs\":[\"1\",\"2\",null],\"materials\":[null],\"rcontent\":\"CCCCCCCCCCCCCCCCCCC\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"after-save\":\"3\",\"_method\":\"PUT\"}', '2020-12-08 23:12:16', '2020-12-08 23:12:16'),
(497, 1, 'admin/remedys/1', 'GET', '127.0.0.1', '[]', '2020-12-08 23:12:16', '2020-12-08 23:12:16'),
(498, 1, 'admin/remedys/1', 'GET', '127.0.0.1', '[]', '2020-12-08 23:14:15', '2020-12-08 23:14:15');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(499, 1, 'admin/remedys/1', 'GET', '127.0.0.1', '[]', '2020-12-08 23:14:37', '2020-12-08 23:14:37'),
(500, 1, 'admin/remedys/1', 'GET', '127.0.0.1', '[]', '2020-12-08 23:14:56', '2020-12-08 23:14:56'),
(501, 1, 'admin/remedys/1', 'GET', '127.0.0.1', '[]', '2020-12-08 23:14:57', '2020-12-08 23:14:57'),
(502, 1, 'admin/remedys/1', 'GET', '127.0.0.1', '[]', '2020-12-08 23:15:02', '2020-12-08 23:15:02'),
(503, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:15:34', '2020-12-08 23:15:34'),
(504, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:15:42', '2020-12-08 23:15:42'),
(505, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '[]', '2020-12-08 23:19:14', '2020-12-08 23:19:14'),
(506, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:19:37', '2020-12-08 23:19:37'),
(507, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:19:43', '2020-12-08 23:19:43'),
(508, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:19:45', '2020-12-08 23:19:45'),
(509, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:20:26', '2020-12-08 23:20:26'),
(510, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:20:30', '2020-12-08 23:20:30'),
(511, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:20:32', '2020-12-08 23:20:32'),
(512, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:21:02', '2020-12-08 23:21:02'),
(513, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:32:50', '2020-12-08 23:32:50'),
(514, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:33:35', '2020-12-08 23:33:35');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(515, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:34:04', '2020-12-08 23:34:04'),
(516, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:34:45', '2020-12-08 23:34:45'),
(517, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:36:39', '2020-12-08 23:36:39'),
(518, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:37:09', '2020-12-08 23:37:09'),
(519, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:37:33', '2020-12-08 23:37:33'),
(520, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:37:46', '2020-12-08 23:37:46'),
(521, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:38:42', '2020-12-08 23:38:42'),
(522, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:41:48', '2020-12-08 23:41:48'),
(523, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:42:20', '2020-12-08 23:42:20'),
(524, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:42:45', '2020-12-08 23:42:45'),
(525, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:42:53', '2020-12-08 23:42:53'),
(526, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:42:55', '2020-12-08 23:42:55'),
(527, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:43:19', '2020-12-08 23:43:19'),
(528, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:52:13', '2020-12-08 23:52:13'),
(529, 1, 'admin/remedys/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-08 23:52:17', '2020-12-08 23:52:17');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(530, 1, 'admin/remedys/4', 'PUT', '127.0.0.1', '{\"rname\":\"WWWW\",\"cats\":[\"3\",null],\"funs\":[\"3\",null],\"materials\":[\"2\",\"3\",null],\"rcontent\":\"cxcxcxc\\r\\nxcxcx\\r\\ncxcxccxcxc\",\"_token\":\"IXnyxxdpOEIN6GUBnCt7Mg9ME3MTr39e00ZrGK1o\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-08 23:52:19', '2020-12-08 23:52:19'),
(531, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-08 23:52:19', '2020-12-08 23:52:19'),
(532, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:13:52', '2020-12-09 00:13:52'),
(533, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:14:06', '2020-12-09 00:14:06'),
(534, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:14:13', '2020-12-09 00:14:13'),
(535, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:14:16', '2020-12-09 00:14:16'),
(536, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:14:31', '2020-12-09 00:14:31'),
(537, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:14:49', '2020-12-09 00:14:49'),
(538, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:15:01', '2020-12-09 00:15:01'),
(539, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:15:28', '2020-12-09 00:15:28'),
(540, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:15:47', '2020-12-09 00:15:47'),
(541, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:15:50', '2020-12-09 00:15:50'),
(542, 2, 'admin', 'GET', '127.0.0.1', '[]', '2020-12-09 00:15:54', '2020-12-09 00:15:54'),
(543, 2, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:15:58', '2020-12-09 00:15:58'),
(544, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:16:03', '2020-12-09 00:16:03');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(545, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-12-09 00:16:13', '2020-12-09 00:16:13'),
(546, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:16:19', '2020-12-09 00:16:19'),
(547, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:16:21', '2020-12-09 00:16:21'),
(548, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:16:23', '2020-12-09 00:16:23'),
(549, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:16:30', '2020-12-09 00:16:30'),
(550, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:16:34', '2020-12-09 00:16:34'),
(551, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:16:38', '2020-12-09 00:16:38'),
(552, 1, 'admin/auth/menu/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:16:40', '2020-12-09 00:16:40'),
(553, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:16:45', '2020-12-09 00:16:45'),
(554, 1, 'admin/auth/menu/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:16:49', '2020-12-09 00:16:49'),
(555, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:16:56', '2020-12-09 00:16:56'),
(556, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"VjJqvUoM0kurKNvtW7ZS7oQhw0AzgkXbsFSCZ3JK\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":8},{\\\"id\\\":9},{\\\"id\\\":11},{\\\"id\\\":10},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":7}]}]\"}', '2020-12-09 00:17:15', '2020-12-09 00:17:15'),
(557, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:17:15', '2020-12-09 00:17:15');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(558, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-09 00:17:16', '2020-12-09 00:17:16'),
(559, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:17:24', '2020-12-09 00:17:24'),
(560, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:17:28', '2020-12-09 00:17:28'),
(561, 2, 'admin', 'GET', '127.0.0.1', '[]', '2020-12-09 00:17:33', '2020-12-09 00:17:33'),
(562, 2, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:17:36', '2020-12-09 00:17:36'),
(563, 2, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:17:37', '2020-12-09 00:17:37'),
(564, 2, 'admin/rcats', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:17:38', '2020-12-09 00:17:38'),
(565, 2, 'admin/rcats', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:17:39', '2020-12-09 00:17:39'),
(566, 2, 'admin/rfuns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:17:42', '2020-12-09 00:17:42'),
(567, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:17:47', '2020-12-09 00:17:47'),
(568, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-12-09 00:18:00', '2020-12-09 00:18:00'),
(569, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:18:05', '2020-12-09 00:18:05'),
(570, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:18:07', '2020-12-09 00:18:07'),
(571, 1, 'admin/auth/permissions/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:18:19', '2020-12-09 00:18:19'),
(572, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:18:23', '2020-12-09 00:18:23');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(573, 1, 'admin/auth/permissions/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:18:33', '2020-12-09 00:18:33'),
(574, 1, 'admin/auth/permissions', 'POST', '127.0.0.1', '{\"slug\":\"\\u504f\\u65b9\",\"name\":\"QQQ\",\"http_method\":[null],\"http_path\":\"\\/admin\\/remedys\\r\\n\\/admin\\/rcats\\r\\n\\/admin\\/rfuns\\r\\n\\/admin\\/rmaterials\",\"_token\":\"wcGYCB1BolYVsD5maAnOnvHdMWOrZ1xhtDlD59z5\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/permissions\"}', '2020-12-09 00:19:13', '2020-12-09 00:19:13'),
(575, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2020-12-09 00:19:13', '2020-12-09 00:19:13'),
(576, 1, 'admin/auth/permissions/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:19:20', '2020-12-09 00:19:20'),
(577, 1, 'admin/auth/permissions/6', 'PUT', '127.0.0.1', '{\"slug\":\"\\u504f\\u65b9\",\"name\":\"\\u504f\\u65b9\",\"http_method\":[null],\"http_path\":\"\\/admin\\/remedys\\r\\n\\/admin\\/rcats\\r\\n\\/admin\\/rfuns\\r\\n\\/admin\\/rmaterials\",\"_token\":\"wcGYCB1BolYVsD5maAnOnvHdMWOrZ1xhtDlD59z5\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/permissions\"}', '2020-12-09 00:19:23', '2020-12-09 00:19:23'),
(578, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2020-12-09 00:19:23', '2020-12-09 00:19:23'),
(579, 1, 'admin/auth/permissions/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:19:28', '2020-12-09 00:19:28'),
(580, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:19:38', '2020-12-09 00:19:38'),
(581, 1, 'admin/auth/menu/8/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:19:40', '2020-12-09 00:19:40');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(582, 1, 'admin/auth/menu/8', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u504f\\u65b9\",\"icon\":\"fa-bars\",\"uri\":\"remedys\",\"roles\":[null],\"permission\":\"\\u504f\\u65b9\",\"_token\":\"wcGYCB1BolYVsD5maAnOnvHdMWOrZ1xhtDlD59z5\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/menu\"}', '2020-12-09 00:19:49', '2020-12-09 00:19:49'),
(583, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-09 00:19:49', '2020-12-09 00:19:49'),
(584, 1, 'admin/auth/menu/9/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:19:52', '2020-12-09 00:19:52'),
(585, 1, 'admin/auth/menu/9', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u5206\\u985e\",\"icon\":\"fa-bars\",\"uri\":\"rcats\",\"roles\":[null],\"permission\":\"\\u504f\\u65b9\",\"_token\":\"wcGYCB1BolYVsD5maAnOnvHdMWOrZ1xhtDlD59z5\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/menu\"}', '2020-12-09 00:19:55', '2020-12-09 00:19:55'),
(586, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-09 00:19:56', '2020-12-09 00:19:56'),
(587, 1, 'admin/auth/menu/9/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:19:58', '2020-12-09 00:19:58'),
(588, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:20:01', '2020-12-09 00:20:01'),
(589, 1, 'admin/auth/menu/11/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:20:03', '2020-12-09 00:20:03');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(590, 1, 'admin/auth/menu/11', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u529f\\u6548\",\"icon\":\"fa-bars\",\"uri\":\"rfuns\",\"roles\":[null],\"permission\":\"\\u504f\\u65b9\",\"_token\":\"wcGYCB1BolYVsD5maAnOnvHdMWOrZ1xhtDlD59z5\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/menu\"}', '2020-12-09 00:20:07', '2020-12-09 00:20:07'),
(591, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-09 00:20:07', '2020-12-09 00:20:07'),
(592, 1, 'admin/auth/menu/10/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:20:09', '2020-12-09 00:20:09'),
(593, 1, 'admin/auth/menu/10', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"\\u7d20\\u6750\",\"icon\":\"fa-bars\",\"uri\":\"rmaterials\",\"roles\":[null],\"permission\":\"\\u504f\\u65b9\",\"_token\":\"wcGYCB1BolYVsD5maAnOnvHdMWOrZ1xhtDlD59z5\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/menu\"}', '2020-12-09 00:20:12', '2020-12-09 00:20:12'),
(594, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-12-09 00:20:12', '2020-12-09 00:20:12'),
(595, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:20:16', '2020-12-09 00:20:16'),
(596, 2, 'admin', 'GET', '127.0.0.1', '[]', '2020-12-09 00:20:26', '2020-12-09 00:20:26'),
(597, 2, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:20:33', '2020-12-09 00:20:33'),
(598, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-12-09 00:20:39', '2020-12-09 00:20:39'),
(599, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:02', '2020-12-09 00:21:02'),
(600, 1, 'admin/auth/users', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:07', '2020-12-09 00:21:07');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(601, 1, 'admin/auth/users/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:09', '2020-12-09 00:21:09'),
(602, 1, 'admin/auth/users/2', 'PUT', '127.0.0.1', '{\"username\":\"user1\",\"name\":\"user1\",\"password\":\"$2y$10$KSRC5iBsdaiIiaQS5ODrZuSCgnPWpWj23yAe1wIftso3WvYa.xAZK\",\"password_confirmation\":\"$2y$10$KSRC5iBsdaiIiaQS5ODrZuSCgnPWpWj23yAe1wIftso3WvYa.xAZK\",\"roles\":[null],\"permissions\":[\"3\",\"6\",null],\"_token\":\"ActWsUoTmLOmpU4lusWzvQn7LHaM76L886qjzVTr\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/users\"}', '2020-12-09 00:21:13', '2020-12-09 00:21:13'),
(603, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2020-12-09 00:21:13', '2020-12-09 00:21:13'),
(604, 2, 'admin', 'GET', '127.0.0.1', '[]', '2020-12-09 00:21:25', '2020-12-09 00:21:25'),
(605, 2, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:27', '2020-12-09 00:21:27'),
(606, 2, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:28', '2020-12-09 00:21:28'),
(607, 2, 'admin/rcats', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:29', '2020-12-09 00:21:29'),
(608, 2, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:30', '2020-12-09 00:21:30'),
(609, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:38', '2020-12-09 00:21:38'),
(610, 1, 'admin/auth/permissions/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:42', '2020-12-09 00:21:42');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(611, 1, 'admin/auth/permissions/6', 'PUT', '127.0.0.1', '{\"slug\":\"\\u504f\\u65b9\",\"name\":\"\\u504f\\u65b9\",\"http_method\":[null],\"http_path\":\"\\/remedys\\r\\n\\/rcats\\r\\n\\/rfuns\\r\\n\\/rmaterials\",\"_token\":\"ActWsUoTmLOmpU4lusWzvQn7LHaM76L886qjzVTr\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/permissions\"}', '2020-12-09 00:21:47', '2020-12-09 00:21:47'),
(612, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2020-12-09 00:21:48', '2020-12-09 00:21:48'),
(613, 2, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:21:50', '2020-12-09 00:21:50'),
(614, 2, 'admin/rcats', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:52', '2020-12-09 00:21:52'),
(615, 2, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:54', '2020-12-09 00:21:54'),
(616, 2, 'admin/rcats', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:55', '2020-12-09 00:21:55'),
(617, 2, 'admin/rfuns', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:56', '2020-12-09 00:21:56'),
(618, 2, 'admin/rmaterials', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:57', '2020-12-09 00:21:57'),
(619, 2, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:21:57', '2020-12-09 00:21:57'),
(620, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:23:34', '2020-12-09 00:23:34'),
(621, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:25:33', '2020-12-09 00:25:33'),
(622, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:25:37', '2020-12-09 00:25:37'),
(623, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '[]', '2020-12-09 00:26:34', '2020-12-09 00:26:34');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(624, 1, 'admin/remedys/1', 'PUT', '127.0.0.1', '{\"rname\":\"RRRRRRRRRRR\",\"isopen\":\"on\",\"cats\":[\"4\",null],\"funs\":[\"1\",\"2\",null],\"materials\":[null],\"rcontent\":\"CCCCCCCCCCCCCCCCCCC\",\"_token\":\"ActWsUoTmLOmpU4lusWzvQn7LHaM76L886qjzVTr\",\"_method\":\"PUT\"}', '2020-12-09 00:26:38', '2020-12-09 00:26:38'),
(625, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:26:38', '2020-12-09 00:26:38'),
(626, 1, 'admin/remedys/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:27:02', '2020-12-09 00:27:02'),
(627, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:27:04', '2020-12-09 00:27:04'),
(628, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:27:18', '2020-12-09 00:27:18'),
(629, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:32:06', '2020-12-09 00:32:06'),
(630, 2, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:32:12', '2020-12-09 00:32:12'),
(631, 2, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:33:54', '2020-12-09 00:33:54'),
(632, 2, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:34:15', '2020-12-09 00:34:15'),
(633, 2, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:36:15', '2020-12-09 00:36:15'),
(634, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:36:25', '2020-12-09 00:36:25'),
(635, 2, 'admin/_handle_action_', 'POST', '127.0.0.1', '{\"_key\":\"1\",\"_model\":\"App_Models_Remedys\",\"_token\":\"7dukh1xnGutRT9AUz6oK6W3fAusfBdEoLsfrwkNe\",\"_action\":\"Encore_Admin_Grid_Actions_Delete\",\"_input\":\"true\"}', '2020-12-09 00:36:39', '2020-12-09 00:36:39'),
(636, 2, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:36:42', '2020-12-09 00:36:42'),
(637, 2, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:36:44', '2020-12-09 00:36:44');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(638, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:49:51', '2020-12-09 00:49:51'),
(639, 1, 'admin/remedys/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:49:55', '2020-12-09 00:49:55'),
(640, 1, 'admin/remedys', 'POST', '127.0.0.1', '{\"rname\":\"dffdfdfdfdfd\",\"isopen\":\"off\",\"cats\":[\"4\",null],\"funs\":[\"1\",null],\"materials\":[\"2\",null],\"rcontent\":\"dfsfsdfsdfsdff\",\"_token\":\"ActWsUoTmLOmpU4lusWzvQn7LHaM76L886qjzVTr\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-09 00:50:03', '2020-12-09 00:50:03'),
(641, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:50:03', '2020-12-09 00:50:03'),
(642, 1, 'admin/remedys/5/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:50:06', '2020-12-09 00:50:06'),
(643, 1, 'admin/remedys/5', 'PUT', '127.0.0.1', '{\"rname\":\"\\u4f60\",\"isopen\":\"off\",\"cats\":[\"4\",null],\"funs\":[\"2\",null],\"materials\":[null],\"rcontent\":\"fdfgdfgfdg\\r\\ngfdgdfgdfgd\\r\\nsdfdsfsdfsdfsd\",\"_token\":\"ActWsUoTmLOmpU4lusWzvQn7LHaM76L886qjzVTr\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-09 00:50:09', '2020-12-09 00:50:09'),
(644, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:50:09', '2020-12-09 00:50:09'),
(645, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:51:50', '2020-12-09 00:51:50'),
(646, 1, 'admin/remedys/5/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:51:52', '2020-12-09 00:51:52');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(647, 1, 'admin/remedys/5', 'PUT', '127.0.0.1', '{\"rname\":\"\\u4f60\",\"isopen\":\"off\",\"cats\":[\"4\",null],\"funs\":[\"2\",null],\"materials\":[null],\"rcontent\":\"fdfgdfgfdg\\r\\ngfdgdfgdfgd\\r\\nsdfdsfsdfsdfsd\",\"_token\":\"ActWsUoTmLOmpU4lusWzvQn7LHaM76L886qjzVTr\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-09 00:51:53', '2020-12-09 00:51:53'),
(648, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:51:54', '2020-12-09 00:51:54'),
(649, 1, 'admin/remedys/5/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:52:12', '2020-12-09 00:52:12'),
(650, 1, 'admin/remedys/5', 'PUT', '127.0.0.1', '{\"rname\":\"\\u4f60\",\"isopen\":\"off\",\"cats\":[\"4\",null],\"funs\":[\"2\",null],\"materials\":[null],\"rcontent\":\"fdfgdfgfdg\\r\\ngfdgdfgdfgd\\r\\nsdfdsfsdfsdfsd\",\"_token\":\"ActWsUoTmLOmpU4lusWzvQn7LHaM76L886qjzVTr\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-09 00:52:14', '2020-12-09 00:52:14'),
(651, 1, 'admin/remedys/5', 'GET', '127.0.0.1', '[]', '2020-12-09 00:52:22', '2020-12-09 00:52:22'),
(652, 1, 'admin/remedys/5/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:52:25', '2020-12-09 00:52:25'),
(653, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:52:26', '2020-12-09 00:52:26'),
(654, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:55:00', '2020-12-09 00:55:00'),
(655, 1, 'admin/remedys/5/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:55:02', '2020-12-09 00:55:02');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(656, 1, 'admin/remedys/5', 'PUT', '127.0.0.1', '{\"rname\":\"\\u4f60\",\"isopen\":\"off\",\"cats\":[\"4\",null],\"funs\":[\"2\",null],\"materials\":[null],\"rcontent\":\"fdfgdfgfdg\\r\\ngfdgdfgdfgd\\r\\nsdfdsfsdfsdfsd\",\"_token\":\"ActWsUoTmLOmpU4lusWzvQn7LHaM76L886qjzVTr\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-09 00:55:03', '2020-12-09 00:55:03'),
(657, 1, 'admin/remedys/5/edit', 'GET', '127.0.0.1', '[]', '2020-12-09 00:55:04', '2020-12-09 00:55:04'),
(658, 1, 'admin/remedys/5/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:56:10', '2020-12-09 00:56:10'),
(659, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 00:56:12', '2020-12-09 00:56:12'),
(660, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 00:59:13', '2020-12-09 00:59:13'),
(661, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 01:02:05', '2020-12-09 01:02:05'),
(662, 2, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 01:02:10', '2020-12-09 01:02:10'),
(663, 1, 'admin/remedys/5/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:02:15', '2020-12-09 01:02:15'),
(664, 1, 'admin/remedys/5', 'PUT', '127.0.0.1', '{\"rname\":\"\\u4f60\",\"isopen\":\"on\",\"cats\":[\"4\",null],\"funs\":[\"2\",null],\"materials\":[null],\"rcontent\":\"fdfgdfgfdg\\r\\ngfdgdfgdfgd\\r\\nsdfdsfsdfsdfsd\",\"_token\":\"ActWsUoTmLOmpU4lusWzvQn7LHaM76L886qjzVTr\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/remedys\"}', '2020-12-09 01:02:17', '2020-12-09 01:02:17'),
(665, 1, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 01:02:17', '2020-12-09 01:02:17'),
(666, 2, 'admin/remedys', 'GET', '127.0.0.1', '[]', '2020-12-09 01:02:19', '2020-12-09 01:02:19');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(667, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:02:47', '2020-12-09 01:02:47'),
(668, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:02:49', '2020-12-09 01:02:49'),
(669, 1, 'admin/remedys/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:02:54', '2020-12-09 01:02:54'),
(670, 2, 'admin/remedys/3', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:03:06', '2020-12-09 01:03:06'),
(671, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:03:17', '2020-12-09 01:03:17'),
(672, 1, 'admin/remedys/3', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:03:20', '2020-12-09 01:03:20'),
(673, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:03:21', '2020-12-09 01:03:21'),
(674, 2, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:03:36', '2020-12-09 01:03:36'),
(675, 2, 'admin/remedys/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:03:40', '2020-12-09 01:03:40'),
(676, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:03:45', '2020-12-09 01:03:45'),
(677, 1, 'admin/auth/permissions/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:03:58', '2020-12-09 01:03:58'),
(678, 1, 'admin/auth/permissions/6', 'PUT', '127.0.0.1', '{\"slug\":\"\\u504f\\u65b9\",\"name\":\"\\u504f\\u65b9\",\"http_method\":[null],\"http_path\":\"\\/remedys\\/*\\r\\n\\/rcats\\/*\\r\\n\\/rfuns\\/*\\r\\n\\/rmaterials\\/*\",\"_token\":\"ActWsUoTmLOmpU4lusWzvQn7LHaM76L886qjzVTr\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/permissions\"}', '2020-12-09 01:04:04', '2020-12-09 01:04:04');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(679, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2020-12-09 01:04:04', '2020-12-09 01:04:04'),
(680, 2, 'admin/remedys/create', 'GET', '127.0.0.1', '[]', '2020-12-09 01:04:06', '2020-12-09 01:04:06'),
(681, 2, 'admin/rmaterials', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:04:09', '2020-12-09 01:04:09'),
(682, 2, 'admin/rmaterials', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:04:12', '2020-12-09 01:04:12'),
(683, 1, 'admin/auth/permissions/6/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:04:15', '2020-12-09 01:04:15'),
(684, 1, 'admin/auth/permissions/6', 'PUT', '127.0.0.1', '{\"slug\":\"\\u504f\\u65b9\",\"name\":\"\\u504f\\u65b9\",\"http_method\":[null],\"http_path\":\"\\/remedys\\/*\\r\\n\\/rcats\\/*\\r\\n\\/rfuns\\/*\\r\\n\\/rmaterials\\/*\\r\\n\\r\\n\\/remedys\\r\\n\\/rcats\\r\\n\\/rfuns\\r\\n\\/rmaterials\",\"_token\":\"ActWsUoTmLOmpU4lusWzvQn7LHaM76L886qjzVTr\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/auth\\/permissions\"}', '2020-12-09 01:04:25', '2020-12-09 01:04:25'),
(685, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '[]', '2020-12-09 01:04:25', '2020-12-09 01:04:25'),
(686, 2, 'admin/rmaterials', 'GET', '127.0.0.1', '[]', '2020-12-09 01:04:28', '2020-12-09 01:04:28'),
(687, 2, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:04:30', '2020-12-09 01:04:30'),
(688, 2, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:04:32', '2020-12-09 01:04:32'),
(689, 2, 'admin/remedys/3', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:04:34', '2020-12-09 01:04:34'),
(690, 2, 'admin/remedys/3', 'GET', '127.0.0.1', '[]', '2020-12-09 01:05:59', '2020-12-09 01:05:59');
INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(691, 2, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:06:01', '2020-12-09 01:06:01'),
(692, 2, 'admin/remedys/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:06:04', '2020-12-09 01:06:04'),
(693, 2, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:06:07', '2020-12-09 01:06:07'),
(694, 2, 'admin/remedys/3', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:06:09', '2020-12-09 01:06:09'),
(695, 2, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:06:11', '2020-12-09 01:06:11'),
(696, 2, 'admin/remedys/4/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:06:13', '2020-12-09 01:06:13'),
(697, 2, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:06:17', '2020-12-09 01:06:17'),
(698, 1, 'admin/remedys', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-12-09 01:06:45', '2020-12-09 01:06:45');

-- --------------------------------------------------------

--
-- 資料表結構 `admin_permissions`
--

DROP TABLE IF EXISTS `admin_permissions`;
CREATE TABLE IF NOT EXISTS `admin_permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_permissions_name_unique` (`name`),
  UNIQUE KEY `admin_permissions_slug_unique` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `admin_permissions`
--

INSERT INTO `admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1, 'All permission', '*', '', '*', NULL, NULL),
(2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL),
(3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL),
(4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL),
(5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL),
(6, '偏方', '偏方', '', '/remedys/*\r\n/rcats/*\r\n/rfuns/*\r\n/rmaterials/*\r\n\r\n/remedys\r\n/rcats\r\n/rfuns\r\n/rmaterials', '2020-12-09 00:19:13', '2020-12-09 01:04:25');

-- --------------------------------------------------------

--
-- 資料表結構 `admin_roles`
--

DROP TABLE IF EXISTS `admin_roles`;
CREATE TABLE IF NOT EXISTS `admin_roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_roles_name_unique` (`name`),
  UNIQUE KEY `admin_roles_slug_unique` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator', '2020-12-06 18:30:50', '2020-12-06 18:30:50');

-- --------------------------------------------------------

--
-- 資料表結構 `admin_role_menu`
--

DROP TABLE IF EXISTS `admin_role_menu`;
CREATE TABLE IF NOT EXISTS `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `admin_role_menu`
--

INSERT INTO `admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `admin_role_permissions`
--

DROP TABLE IF EXISTS `admin_role_permissions`;
CREATE TABLE IF NOT EXISTS `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `admin_role_permissions`
--

INSERT INTO `admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `admin_role_users`
--

DROP TABLE IF EXISTS `admin_role_users`;
CREATE TABLE IF NOT EXISTS `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `admin_role_users`
--

INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE IF NOT EXISTS `admin_users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_username_unique` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `admin_users`
--

INSERT INTO `admin_users` (`id`, `username`, `password`, `name`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$Hbdh7y80Iqz3AH/HeqcK1OhDE1eRyiT6RiS0F0fNYaoTiHZv6doOe', 'Administrator', NULL, 'w0rkwweldmXA4f7h9H9PE0MfaZOnoX4FLobB1ojsZV2l9NoABeBmsiZc2kyN', '2020-12-06 18:30:50', '2020-12-06 19:12:13'),
(2, 'user1', '$2y$10$KSRC5iBsdaiIiaQS5ODrZuSCgnPWpWj23yAe1wIftso3WvYa.xAZK', 'user1', NULL, '6lK4qRJt6fhPNtc1Dt8ntqvDbgal6odhYTubjZgVn87A4NIEGyzk9VY9Zlrg', '2020-12-06 19:23:58', '2020-12-06 19:23:58');

-- --------------------------------------------------------

--
-- 資料表結構 `admin_user_permissions`
--

DROP TABLE IF EXISTS `admin_user_permissions`;
CREATE TABLE IF NOT EXISTS `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `admin_user_permissions`
--

INSERT INTO `admin_user_permissions` (`user_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(2, 3, NULL, NULL),
(2, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`) USING HASH
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_04_173148_create_admin_tables', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- 資料表結構 `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `rcats`
--

DROP TABLE IF EXISTS `rcats`;
CREATE TABLE IF NOT EXISTS `rcats` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(255) CHARACTER SET utf16 NOT NULL,
  `auid` int(11) NOT NULL,
  `atime` datetime NOT NULL,
  `muid` int(11) NOT NULL,
  `mtime` datetime NOT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `rcats`
--

INSERT INTO `rcats` (`cid`, `cname`, `auid`, `atime`, `muid`, `mtime`) VALUES
(4, 'EEE', 2, '2020-12-09 00:00:00', 1, '2020-12-09 03:15:34'),
(3, 'AAA', 2, '2020-12-09 00:00:00', 2, '2020-12-09 00:00:00'),
(5, 'QQQ', 1, '2020-12-09 03:15:42', 1, '2020-12-09 03:15:42');

-- --------------------------------------------------------

--
-- 資料表結構 `remedys`
--

DROP TABLE IF EXISTS `remedys`;
CREATE TABLE IF NOT EXISTS `remedys` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `rname` varchar(255) CHARACTER SET utf16 NOT NULL,
  `rcontent` text CHARACTER SET utf16 NOT NULL,
  `isopen` int(11) NOT NULL DEFAULT 0,
  `auid` int(11) NOT NULL,
  `atime` datetime NOT NULL,
  `muid` int(11) NOT NULL,
  `mtime` datetime NOT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `remedys`
--

INSERT INTO `remedys` (`rid`, `rname`, `rcontent`, `isopen`, `auid`, `atime`, `muid`, `mtime`) VALUES
(3, '你', 'fdfgdfgfdg\r\ngfdgdfgdfgd\r\nsdfdsfsdfsdfsd', 1, 1, '2020-12-09 05:55:53', 1, '2020-12-09 09:02:17'),
(4, 'WWWW', 'cxcxcxc\r\nxcxcx\r\ncxcxccxcxc', 0, 2, '2020-12-09 06:56:36', 1, '2020-12-09 07:52:19'),
(5, 'dffdfdfdfdfd', 'dfsfsdfsdfsdff', 0, 1, '2020-12-09 08:50:03', 1, '2020-12-09 08:50:03');

-- --------------------------------------------------------

--
-- 資料表結構 `rfuns`
--

DROP TABLE IF EXISTS `rfuns`;
CREATE TABLE IF NOT EXISTS `rfuns` (
  `fid` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) CHARACTER SET utf16 NOT NULL,
  `auid` int(11) NOT NULL,
  `atime` datetime NOT NULL,
  `muid` int(11) NOT NULL,
  `mtime` datetime NOT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `rfuns`
--

INSERT INTO `rfuns` (`fid`, `fname`, `auid`, `atime`, `muid`, `mtime`) VALUES
(1, '功能1', 1, '2020-12-09 04:03:01', 1, '2020-12-09 04:03:01'),
(2, '功能2', 1, '2020-12-09 04:03:05', 1, '2020-12-09 04:03:05'),
(3, '功能3', 1, '2020-12-09 04:03:09', 1, '2020-12-09 04:03:09');

-- --------------------------------------------------------

--
-- 資料表結構 `rmaterials`
--

DROP TABLE IF EXISTS `rmaterials`;
CREATE TABLE IF NOT EXISTS `rmaterials` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `mname` varchar(255) CHARACTER SET utf16 NOT NULL,
  `auid` int(11) NOT NULL,
  `atime` datetime NOT NULL,
  `muid` int(11) NOT NULL,
  `mtime` datetime NOT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `rmaterials`
--

INSERT INTO `rmaterials` (`mid`, `mname`, `auid`, `atime`, `muid`, `mtime`) VALUES
(1, '當歸', 1, '2020-12-09 06:53:20', 1, '2020-12-09 06:53:20'),
(2, '枸杞', 1, '2020-12-09 06:53:28', 1, '2020-12-09 06:53:28'),
(3, '艾草', 1, '2020-12-09 06:53:40', 1, '2020-12-09 06:53:40');

-- --------------------------------------------------------

--
-- 資料表結構 `r_cats_remedys`
--

DROP TABLE IF EXISTS `r_cats_remedys`;
CREATE TABLE IF NOT EXISTS `r_cats_remedys` (
  `rcid` int(11) NOT NULL AUTO_INCREMENT,
  `remedys_rid` int(11) NOT NULL,
  `r_cats_cid` int(11) NOT NULL,
  PRIMARY KEY (`rcid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `r_cats_remedys`
--

INSERT INTO `r_cats_remedys` (`rcid`, `remedys_rid`, `r_cats_cid`) VALUES
(1, 1, 4),
(3, 2, 3),
(4, 3, 4),
(5, 4, 3),
(6, 5, 4);

-- --------------------------------------------------------

--
-- 資料表結構 `r_funs_remedys`
--

DROP TABLE IF EXISTS `r_funs_remedys`;
CREATE TABLE IF NOT EXISTS `r_funs_remedys` (
  `rfid` int(11) NOT NULL AUTO_INCREMENT,
  `remedys_rid` int(11) NOT NULL,
  `r_funs_fid` int(11) NOT NULL,
  PRIMARY KEY (`rfid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `r_funs_remedys`
--

INSERT INTO `r_funs_remedys` (`rfid`, `remedys_rid`, `r_funs_fid`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 3),
(5, 3, 2),
(6, 4, 3),
(7, 5, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `r_materials_remedys`
--

DROP TABLE IF EXISTS `r_materials_remedys`;
CREATE TABLE IF NOT EXISTS `r_materials_remedys` (
  `rmid` int(11) NOT NULL AUTO_INCREMENT,
  `remedys_rid` int(11) NOT NULL,
  `r_materials_mid` int(11) NOT NULL,
  PRIMARY KEY (`rmid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- 傾印資料表的資料 `r_materials_remedys`
--

INSERT INTO `r_materials_remedys` (`rmid`, `remedys_rid`, `r_materials_mid`) VALUES
(1, 4, 2),
(2, 4, 3),
(3, 5, 2);

-- --------------------------------------------------------

--
-- 資料表結構 `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`) USING HASH
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
