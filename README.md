# 簡易偏方功能 #

簡易偏方功能  
基於 Laravel Framework 8.17.2
基於 laravel-admin 1.8  

## 安裝參考 ##

https://blog.gtwang.org/linux/ubuntu-linux-laravel-nginx-mariadb-installation-tutorial/

### What is this repository for? ###


1. 可以新增偏方
2. 偏方可以設定 分類 素材 功效
3. 列表可以搜尋 分類 素材 功效
4. 可以設定公開.其他帳號可以看得到

### 基礎啟用方式 ###

1. 安裝PHP的套件管理composer，可參考 http://blog.tonycube.com/2016/12/composer-php.html
1. 移動到 /home_remedy/
1. 安裝需要套件，類似 python 的 pip，指令為 composer install
1. 先確認DB有匯入，預設資料庫為home_remedy.sql
1. 確認DB連線資訊正確，檔案在 /home_remedy/config/database.php，確認connections參數中的mysql設定
1. 簡易啟用，使用 PHP指令為 php artisan serve




