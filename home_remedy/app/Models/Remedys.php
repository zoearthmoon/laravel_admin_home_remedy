<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Remedys extends Model
{
    use HasFactory;
    protected $table = 'remedys';
    public $timestamps = true;

    protected $primaryKey = 'rid';

    const CREATED_AT = 'atime';
    const UPDATED_AT = 'mtime';

    public function newQuery($excludeDeleted = false) {
        $nQ = parent::newQuery($excludeDeleted)
            ->where(function ($query) {
                $query->where('auid', '=', auth()->id())
                    ->orWhere('isopen', '=', '1');
            });
        return $nQ;
    }

    public function save(array $options = array())
    {
        $this->auid = $this->auid > 0 ? $this->auid:auth()->id();
        $this->muid = auth()->id();
        parent::save($options);
    }

    public function cats()
    {
        return $this->belongsToMany(RCats::class);
    }
    public function funs()
    {
        return $this->belongsToMany(RFuns::class);
    }
    public function materials()
    {
        return $this->belongsToMany(RMaterials::class);
    }
    
    public function r_cats_remedys()
    {
        return $this->hasMany(RCatsRemedys::class, 'remedys_rid');
    }
    public function r_funs_remedys()
    {
        return $this->hasMany(RFunsRemedys::class, 'remedys_rid');
    }
    public function r_materials_remedys()
    {
        return $this->hasMany(RMaterialsRemedys::class, 'remedys_rid');
    }

    public function aname()
    {
        return $this->hasMany(AdminUsers::class, 'id', 'auid');
    }
    public function mname()
    {
        return $this->hasMany(AdminUsers::class, 'id', 'muid');
    }
    
}
