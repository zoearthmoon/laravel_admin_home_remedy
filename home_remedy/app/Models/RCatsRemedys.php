<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RCatsRemedys extends Model
{
    use HasFactory;
    protected $table = 'r_cats_remedys';
    public $timestamps = false;

    protected $primaryKey = 'rcid';

    public function remedys_rid()
    {
        return $this->belongsToMany(RCats::class);
    }
    public function r_cats_cid()
    {
        return $this->belongsToMany(Remedys::class);
    }
}
