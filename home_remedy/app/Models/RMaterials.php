<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RMaterials extends Model
{
    use HasFactory;
    protected $table = 'rmaterials';
    public $timestamps = true;

    protected $primaryKey = 'mid';

    const CREATED_AT = 'atime';
    const UPDATED_AT = 'mtime';

    public function save(array $options = array())
    {
        $this->auid = $this->auid > 0 ? $this->auid:auth()->id();
        $this->muid = auth()->id();
        parent::save($options);
    }
    public function remedys()
    {
        return $this->belongsToMany(Remedys::class);
    }
}
