<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RMaterialsRemedys extends Model
{
    use HasFactory;
    protected $table = 'r_materials_remedys';
    public $timestamps = false;

    protected $primaryKey = 'rmid';

    public function remedys_rid()
    {
        return $this->belongsToMany(RMaterials::class);
    }
    public function r_materials_mid()
    {
        return $this->belongsToMany(Remedys::class);
    }
}
