<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RFunsRemedys extends Model
{
    use HasFactory;
    protected $table = 'r_funs_remedys';
    public $timestamps = false;

    protected $primaryKey = 'rfid';

    public function remedys_rid()
    {
        return $this->belongsToMany(RFuns::class);
    }
    public function r_funs_fid()
    {
        return $this->belongsToMany(Remedys::class);
    }
}
