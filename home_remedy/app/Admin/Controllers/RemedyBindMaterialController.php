<?php

namespace App\Admin\Controllers;

use App\Models\RemedyBindMaterial;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class RemedyBindMaterialController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'RemedyBindMaterial';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new RemedyBindMaterial());

        $grid->column('rmid', __('Rmid'));
        $grid->column('rid', __('Rid'));
        $grid->column('mid', __('Mid'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(RemedyBindMaterial::findOrFail($id));

        $show->field('rmid', __('Rmid'));
        $show->field('rid', __('Rid'));
        $show->field('mid', __('Mid'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new RemedyBindMaterial());

        $form->number('rmid', __('Rmid'));
        $form->number('rid', __('Rid'));
        $form->number('mid', __('Mid'));

        return $form;
    }
}
