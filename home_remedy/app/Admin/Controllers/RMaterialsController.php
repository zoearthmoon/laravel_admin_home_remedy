<?php

namespace App\Admin\Controllers;

use App\Models\RMaterials;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class RMaterialsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'RMaterials';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new RMaterials());

        $grid->column('mid', __('Mid'));
        $grid->column('mname', __('Mname'));
        $grid->column('auid', __('Auid'));
        $grid->column('atime', __('Atime'));
        $grid->column('muid', __('Muid'));
        $grid->column('mtime', __('Mtime'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(RMaterials::findOrFail($id));

        $show->field('mid', __('Mid'));
        $show->field('mname', __('Mname'));
        $show->field('auid', __('Auid'));
        $show->field('atime', __('Atime'));
        $show->field('muid', __('Muid'));
        $show->field('mtime', __('Mtime'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new RMaterials());

        
        $form->text('mname', __('Mname'));

        return $form;
    }
}
