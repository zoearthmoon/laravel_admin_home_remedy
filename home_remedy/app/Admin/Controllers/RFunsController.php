<?php

namespace App\Admin\Controllers;

use App\Models\RFuns;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class RFunsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'RFuns';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new RFuns());

        $grid->column('fid', __('Fid'));
        $grid->column('fname', __('Fname'));
        $grid->column('auid', __('Auid'));
        $grid->column('atime', __('Atime'));
        $grid->column('muid', __('Muid'));
        $grid->column('mtime', __('Mtime'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(RFuns::findOrFail($id));

        $show->field('fid', __('Fid'));
        $show->field('fname', __('Fname'));
        $show->field('auid', __('Auid'));
        $show->field('atime', __('Atime'));
        $show->field('muid', __('Muid'));
        $show->field('mtime', __('Mtime'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new RFuns());

        $form->text('fname', __('Fname'));

        return $form;
    }
}
