<?php

namespace App\Admin\Controllers;

use App\Models\RCats;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class RCatsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'RCats';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new RCats());

        $grid->column('cid', __('Cid'));
        $grid->column('cname', __('Cname'));
        $grid->column('auid', __('Auid'));
        $grid->column('atime', __('Atime'));
        $grid->column('muid', __('Muid'));
        $grid->column('mtime', __('Mtime'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(RCats::findOrFail($id));

        $show->field('cid', __('Cid'));
        $show->field('cname', __('Cname'));
        $show->field('auid', __('Auid'));
        $show->field('atime', __('Atime'));
        $show->field('muid', __('Muid'));
        $show->field('mtime', __('Mtime'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new RCats());

        $form->text('cname', __('Cname'));

        return $form;
    }
}
