<?php

namespace App\Admin\Controllers;

use App\Models\RemedyBindFun;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class RemedyBindFunController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'RemedyBindFun';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new RemedyBindFun());

        $grid->column('rfid', __('Rfid'));
        $grid->column('rid', __('Rid'));
        $grid->column('fid', __('Fid'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(RemedyBindFun::findOrFail($id));

        $show->field('rfid', __('Rfid'));
        $show->field('rid', __('Rid'));
        $show->field('fid', __('Fid'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new RemedyBindFun());

        $form->number('rfid', __('Rfid'));
        $form->number('rid', __('Rid'));
        $form->number('fid', __('Fid'));

        return $form;
    }
}
