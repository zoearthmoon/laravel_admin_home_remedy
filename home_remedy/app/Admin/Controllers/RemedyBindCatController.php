<?php

namespace App\Admin\Controllers;

use App\Models\RemedyBindCat;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class RemedyBindCatController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'RemedyBindCat';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new RemedyBindCat());

        $grid->column('rcid', __('Rcid'));
        $grid->column('rid', __('Rid'));
        $grid->column('cid', __('Cid'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(RemedyBindCat::findOrFail($id));

        $show->field('rcid', __('Rcid'));
        $show->field('rid', __('Rid'));
        $show->field('cid', __('Cid'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new RemedyBindCat());

        $form->number('rid', __('Rid'));
        $form->number('cid', __('Cid'));

        return $form;
    }
}
