<?php

namespace App\Admin\Controllers;

use App\Models\AdminUsers;
use App\Models\Remedys;
use App\Models\RCats;
use App\Models\RFuns;
use App\Models\RMaterials;

use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Database\Eloquent\Collection;
use Encore\Admin\Layout\Content;

use Encore\Admin\Layout\Row;

use App\Admin\Selectable\Users;

function showDatetime10($atime){
    return substr($atime,0,10);
}

function GetUidNames($auids){
    $rturnDs = array();
    foreach ($auids as $idx=>$uid) {
        $user = AdminUsers::where('id', $uid)->first();
        if ($user){
            $rturnDs[] = $user->name;
        } else {
            $rturnDs[] = "N";
        }
        
    }
    return $rturnDs;
}

class RemedysController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Remedys 偏方';

    /*
    public function index(Content $content)
    {
        $content->row(function(Row $row) {
            $row->column(4,RemedysController::grid());
            $row->column(8, 'bar');
        });
        return $content;
    }
    */
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Remedys());
        $grid->disableExport();#禁用导出数据按钮
        $grid->disableRowSelector();#禁用行选择checkbox
        /*
        $grid->model()->collection(function (Collection $collection) {
            $auids = $collection->pluck('auid');
            $data = GetUidNames($auids);
            foreach($collection as $index => $item) {
                print_r("TEST 155515151 = ".$data[$index]);
                $item->auid = $data[$index];
            }
            return $collection;
        });
        */
        $grid->column('rid', __('rid'))->sortable();
        $grid->column('rname', __('名稱'))->sortable();
        $grid->column('isopen', __('公開'))->using(['0' => '否', '1' => '是']);

        $grid->column('cats', __('分類'))->display(function ($cats) {
            $str = "";
            foreach ($cats as $idx=>$cat){
                
                $str .= "<span class='label label-warning'>".$cat['cname']."</span>";
            }
            return $str;
        });
        $grid->column('funs', __('功能'))->display(function ($funs) {
            $str = "";
            foreach ($funs as $idx=>$fun){
                
                $str .= "<span class='label label-info'>".$fun['fname']."</span>";
            }
            return $str;
        });
        $grid->column('materials', __('素材'))->display(function ($materials) {
            $str = "";
            foreach ($materials as $idx=>$material){
                
                $str .= "<span class='label label-success'>".$material['mname']."</span>";
            }
            return $str;
        });

        $grid->column('rcontent', __('內容'))->limit(100);
        $grid->column('aname', __('新增者'))->display(function ($admin_users) {
            #print_r($admin_users);
            return $admin_users[0]['name'];
        });

        $grid->column('atime', __('新增時間'))->display(function ($atime) {
            return showDatetime10($atime);
        })->sortable();
        $grid->column('mname', __('修改者'))->display(function ($admin_users) {
            return $admin_users[0]['name'];
        });;
        $grid->column('mtime', __('修改時間'))->display(function ($mtime) {
            return showDatetime10($mtime);
        })->sortable();

        
        $grid->filter(function ($filter) {
            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            $filter->contains('rname', 'rname');

            $rcats = array();
            foreach (RCats::all() as $rcat) {
                $rcats[$rcat->cid] = $rcat->cname;
            }
            $filter->in('r_cats_remedys.r_cats_cid', 'cat')->checkbox(
                $rcats
            );

            $rfuns = array();
            foreach (RFuns::all() as $rfun) {
                $rfuns[$rfun->fid] = $rfun->fname;
            }
            $filter->in('r_funs_remedys.r_funs_fid', 'fun')->checkbox(
                $rfuns
            );

            $rmaterials = array();
            foreach (RMaterials::all() as $rmaterial) {
                $rmaterials[$rmaterial->mid] = $rmaterial->mname;
            }
            $filter->in('r_materials_remedys.r_materials_mid', 'material')->checkbox(
                $rmaterials
            );
        });
        #$grid->quickSearch();
        #$grid->model()->where('rname', 'like', "%{$input}%");

        $grid->actions(function (Grid\Displayers\Actions $actions) {
            // 当前行的数据数组
            if ($actions->row->auid != auth()->id()){
                // 去掉删除
                $actions->disableDelete();

                // 去掉编辑
                $actions->disableEdit();
            }
            // 去掉查看
            //$actions->disableView();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Remedys::findOrFail($id));

        $show->field('rid', __('rid'));
        $show->field('rname', __('rname'));
        $show->field('isopen', __('isopen'));
        $show->field('rcontent', __('rcontent'));
        $show->field('auid', __('auid'));
        $show->field('atime', __('atime'));
        $show->field('muid', __('muid'));
        $show->field('mtime', __('mtime'));

        $show->panel()
        ->tools(function ($tools) {
            $tools->disableEdit();
            //$tools->disableList();
            $tools->disableDelete();
        });;

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Remedys());

        $form->text('rname', __('名稱'));
        $form->switch('isopen', __('公開'));
        $form->multipleSelect('cats', '分類')->options(RCats::all()->pluck('cname', 'cid'));
        $form->multipleSelect('funs', '功效')->options(RFuns::all()->pluck('fname', 'fid'));
        $form->multipleSelect('materials', '素材')->options(RMaterials::all()->pluck('mname', 'mid'));
        $form->textarea('rcontent', __('描述'));
        
        return $form;
    }
}
