<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');

    $router->resource('users', UserController::class); #添加路由
    $router->resource('remedys', RemedysController::class);
    $router->resource('rcats', RCatsController::class);
    $router->resource('rfuns', RFunsController::class);
    $router->resource('rmaterials', RMaterialsController::class);

});
